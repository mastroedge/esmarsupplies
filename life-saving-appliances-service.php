<!DOCTYPE html>
<html lang="en">

<head>
    <title>Best LIFE SAVING APPLIANCES Servicing company in all major Ports of UAE - Abu Dhabi, Dubai, Sharjah, Fujairah
    </title>
    <meta name="description"
        content="ESMAR is the best Life Saving Appliances Servicing Company in UAE - Abu Dhabi, Dubai, Sharjah, Fujairah Ports.ESMAR have skilled technicians and Engineers , for doing SCBA Sets Inspection, EEBD Sets Inspection,Immersion Suits Inspection, Lifejackets Inspection.">
    <meta name="author" content="">

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8 bottom-sm-30">
                        <div class="entry-item">
                            <div class="entry-header">

                                <div class="main-heading text-left">
                                    <h2 class="heading-title wow fadeInDown" data-wow-duration="0.6s"
                                        data-wow-delay="0.6s">LIFE SAVING <span class="text-orange">APPLIANCES
                                            SERVICE</span></h2>
                                </div>
                                <div class="entry-media bottom-20">
                                    <a href="#"><img src="assets/images/img/new_img/service-02.jpg"
                                            class="img-responsive" alt=""></a>
                                </div>
                                <div class="entry-content">
                                    <p>The Qualified engineers and skilled technicians of ESMAR are trained to service
                                        all Life Saving equipment’s in accordance to IMO and SOLAS regulations.Our
                                        service and repair department can inspect and repair survival suits, floatation
                                        and safety equipment to ensure full operational capabilities. Our service
                                        stations is well equipped for Inspection, Service and Certification of the below
                                        equipment’s. We go beyond the statutory and technical requirements with in-house
                                        testing of equipment to really find out how the equipment performs at sea.</p>
                                    <ul class="listing-normal">
                                        <li>SCBA Sets Inspection / Function Test / Cylinder Inspection, Refilling /
                                            Hydrotest.</li>
                                        <li> EEBD Sets Inspection / Function Test / Cylinder Inspection / Hydrotest.
                                        </li>
                                        <li> Immersion Suits Inspection / Pressure Test / 3 Year Pressure leak test.
                                        </li>
                                        <li> Lifejackets Inspection.</li>
                                        <li> Resuscitator Annual Service / Medical Oxygen Cylinders Inspection /
                                            Refilling / Hydrotest.</li>
                                        <li> BA Compressor Air quality test.</li>

                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Services</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="liferaft-service">Liferaft Service</a></li>
                                        <li class="active"><a href="life-saving-appliances-service">Life Saving
                                                Appliances Service</a></li>
                                        <li><a href="fire-fighting-service">Fire Fighting Appliances Service</a></li>
                                        <li><a href="lifeboat-rescue-service">Lifeboat / Rescue Boat Service</a></li>
                                        <li><a href="trouble-shooting-service">Calibration / Trouble Shooting</a></li>
                                        <li><a href="hydraulic-fittings">Hydraulic and Pneumatic – Hoses And
                                                Fittings.</a></li>
                                    </ul>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>