<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="ESMAR Supplies Provides SCBA Service in Dubai, Abu Dhabi and Fujairah.">
    <meta name="author" content="">
    <title>SCBA Service | SCBA Service in Dubai, Abu Dhabi and Fujairah</title>
    <link rel="canonical" href="https://esmarsupplies.com/contact" />
    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8 bottom-sm-30">
                        <div class="entry-item">
                            <div class="entry-header">

                                <div class="heading-area">
                                    <h3 class="large bottom-0 heading-title"><span>CONTACT US</span></h3>

                                </div>
                                <div>
                                    <div class="heading-area top-40">
                                        <h3 class="heading-title"><span>ABU DHABI</span></h3>
                                    </div>
                                    <div class="clearfix">

                                    </div>
                                    <div class="job-board">
                                        <ul class="list-unstyled bottom-0 jobboard">
                                            <li class="clearfix">

                                                <div class="job-info">

                                                    <span class="city"><b>Elite Safety & Marine Supplies L L C<br />
                                                            Store#58, Plot#8, Musaffah Industrial Area,<br />
                                                            M20, Abu Dhabi, UAE
                                                        </b></span><br>
                                                    <span class="city">Tel: +971 2 554 4315</span><br>
                                                    <span class="city">Fax: +971 2 554 3122</span><br>
                                                    <span class="city">P. O. box 92839</span><br>
                                                    <span class="city">Email: <a href="mailto:sales@esmarsupplies.com"
                                                            target="_top">sales@esmarsupplies.com</a></span><br>
                                                    <div style="margin-top: 15px;">
                                                        <iframe
                                                            src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d3634.375225546459!2d54.472725!3d24.368247!3m2!1i1024!2i768!4f13.1!2m1!1sELITE+SAFETY+%26+MARINE+SUPPLIES+L+L+C!5e0!3m2!1sen!2sin!4v1427037974074"
                                                            width="680" height="270" frameborder="0"
                                                            style="border:0"></iframe>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="heading-area top-40">
                                        <h3 class="heading-title"><span>DUBAI</span></h3>
                                    </div>
                                    <div class="clearfix">

                                    </div>
                                    <div class="job-board">
                                        <ul class="list-unstyled bottom-0 jobboard">
                                            <li class="clearfix">

                                                <div class="job-info">
                                                    <span class="city"><b>Emprise Safety & Marine Supplies L L C<br />
                                                            Warehouse# W122B, Dubai Maritime City,<br />
                                                            Dubai,UAE</b></span><br>
                                                    <span class="city">Tel: +971 4 554 6090</span><br>
                                                    <span class="city">Fax: +971 4 554 6091</span><br>
                                                    <span class="city">P.O.Box: 121120</span><br>
                                                    <span class="city">Email: <a href="mailto:info@esmarsupplies.com"
                                                            target="_top">info@esmarsupplies.com</a></span><br>
                                                    <div style="margin-top: 15px;">
                                                        <iframe
                                                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d451.0220827055585!2d55.26524681779099!3d25.26464197435467!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjXCsDE1JzUzLjgiTiA1NcKwMTUnNTQuOCJF!5e0!3m2!1sen!2sae!4v1429496642109"
                                                            width="680" height="270" frameborder="0"
                                                            style="border:0"></iframe>

                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>


                                    <div class="heading-area top-40">
                                        <h3 class="heading-title"><span>FUJAIRAH</span></h3>
                                    </div>
                                    <div class="clearfix">

                                    </div>
                                    <div class="job-board">
                                        <ul class="list-unstyled bottom-0 jobboard">
                                            <li class="clearfix">

                                                <div class="job-info">
                                                    <span class="city"><b>Elite Safety & Marine Supplies L L C<br />
                                                            Plot#18, Inside Fujairah Port
                                                        </b></span><br>
                                                    <span class="city">Tel: +971 2 5544315</span><br>
                                                    <span class="city">Mob: +971 589989825, +971 526398418</span><br>
                                                    <span class="city">Fax: +971 2 5543122</span><br>
                                                    <span class="city">Email: <a href="mailto:service@esmarsupplies.com"
                                                            target="_top">service@esmarsupplies.com</a></span><br>
                                                    <div style="margin-top: 15px;">
                                                        <iframe
                                                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14443.859268579661!2d56.3547839!3d25.1706658!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x810ae91c1c33ae9e!2sElite+Safety+%26+Marine+Supplies+L.L.C-+Fujairah+Branch!5e0!3m2!1sen!2sin!4v1564852502014!5m2!1sen!2sin"
                                                            width="680" height="270" frameborder="0" style="border:0"
                                                            allowfullscreen></iframe>

                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="sidebar" class="col-md-4">
                        <img style="width:100%; margin-bottom: 15px;"
                            src="assets/images/img/new_img/about_image-2.jpg" />
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>

</body>

</html>