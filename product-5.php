<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div id="product-detail">
                            <div class="row">
                                <div class="col-md-7 bottom-sm-30">
                                    <div class="product-images">
                                        <img src="assets/images/img/product_041_a.jpg" alt="" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h2 class="product-title large thin">
                                        Inflatable Life Jackets
                                    </h2>
                                    <div class="product-review bottom-10">
                                        <span class="star-rating star-rating-4"></span>
                                    </div>

                                    <div class="product-amount clearfix">

                                        <div class="field">
                                            <button class="button"><a href="enquiry">Enquire Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content top-40">
                                <div class="tabs main border clearfix">
                                    <ul class="tabNavigation list-unstyled bottom-0 clearfix">
                                        <li class="active"><a data-toggle="tab" href="#tab1dc11"><i
                                                    class="fa fa-align-justify icon-left"></i> Product Description</a>
                                        </li>
                                    </ul>
                                    <div id="tab1dc11" class="tabs-container active">

                                        <p>
                                            <span span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;Approved
                                                    by ISO &amp; SOLAS</strong></span>
                                        </p>
                                        <p>
                                            <span
                                                span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">&nbsp;<strong>one
                                                    or two inflation chambers</strong>, <strong>manual</strong> or
                                                <strong>automatic</strong> <strong>inflation system</strong>,</span>
                                        </p>
                                        <p>
                                            <span span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">a
                                                <strong>CO2 gas cylinder</strong>, an <strong>oral tube</strong> and the
                                                necessary <strong>harnesses</strong> or <strong>straps</strong></span>
                                        </p>
                                        <p>
                                            <span span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">&nbsp;made
                                                of <strong>durable nylon fabric</strong> with <strong>PU
                                                    coating</strong>,</span>
                                        </p>
                                        <p>
                                            <span span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">the
                                                <strong>single or twin seams</strong> are welded,</span>
                                        </p>
                                        <p>
                                            <br />
                                            <strong><span
                                                    span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">Versions
                                                    of Inflatable life jackets</span></strong><br />
                                            <br />
                                            - <strong>Manual or automatic inflation system</strong>.<br />
                                            - With or without <strong>Integral safety harness</strong>.
                                        </p>
                                        <p>
                                            <span span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">ISO 12402-3
                                                Approved</span>
                                        </p>
                                        <p>
                                            <span span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">&nbsp;Rated
                                                150N but exceeds 171N buoyancy</span>
                                        </p>
                                        <p>
                                            <span
                                                span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">&nbsp;Rounded
                                                comfortable neck</span>
                                        </p>
                                        <p>
                                            <span span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">&nbsp;Zip
                                                closure</span>
                                        </p>
                                        <p>
                                            <span
                                                span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">&nbsp;Integral
                                                removable crotch strap</span>
                                        </p>
                                        <p>
                                            <span span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">&nbsp;Light
                                                fitting point</span>
                                        </p>
                                        <p>
                                            <span
                                                span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">&nbsp;Integral
                                                sprayhood</span>
                                        </p>
                                        <p>
                                            <span span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">&nbsp;2 x
                                                integral lifting beckets</span>
                                        </p>
                                        <p>
                                            <span span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">2/3
                                                stainless steel bar buckle</span>
                                        </p>
                                        <p>
                                            <span span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">Reflective
                                                tape</span>
                                        </p>
                                        <p>
                                            <span
                                                span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">&nbsp;Whistle</span>
                                            &nbsp;38g gas cylinder &nbsp;Oral tube
                                        </p>
                                        <p>
                                            <span span="\&quot;\\&quot;\\\\&quot;\\\\&quot;\\&quot;\&quot;">High
                                                visibility stole</span>
                                        </p>




                                        <p><strong>Certificates :</strong> Information not available</p>
                                    </div>
                                    <div id="tab2dc22" class="tabs-container">
                                        <p>
                                            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
                                            inceptos himenaeos. Nam nec ante. Sed lacinia, urna non tincidunt mattis,
                                            tortor neque adipiscing diam, a cursus ipsum ante quis turpis. Nulla
                                            facilisi.
                                        </p>
                                    </div>
                                    <div id="tab3dc33" class="tabs-container">
                                        <p>
                                            Suspendisse in justo eu magna luctus suscipit. Sed lectus. Integer euismod
                                            lacus luctus magna. Quisque cursus, metus vitae pharetra auctor, sem massa
                                            mattis sem, at interdum magna augue eget diam.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Life Saving</a></li>
                                        <li><a href="firefighting">Fire Fighting</a></li>
                                        <li><a href="chemical-spill">Oil Spill Response</a></li>
                                        <li><a href="ship-deck">Ship Deck Equipments</a></li>
                                        <li><a href="respiratory-protection">Industrial Safety Equipments</a></li>
                                        <li><a href="posters-signs">Posters and Safety Signs</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>