<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div id="product-detail">
                            <div class="row">
                                <div class="col-md-7 bottom-sm-30">
                                    <div class="product-images">
                                        <img src="assets/images/img/product_3_fire-big.jpg" alt=""
                                            class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h2 class="product-title large thin">Spanners for Hydrant</h2>
                                    <div class="product-review bottom-10">
                                        <span class="star-rating star-rating-4"></span>
                                    </div>

                                    <p class="top-20 bottom-20">Product Description</p>
                                    <div class="product-amount clearfix">

                                        <div class="field">
                                            <button class="button"><a href="enquiry">Enquire Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content top-40">
                                <div class="tabs main border clearfix">
                                    <ul class="tabNavigation list-unstyled bottom-0 clearfix">
                                        <li class="active"><a data-toggle="tab" href="#tab1dc11"><i
                                                    class="fa fa-align-justify icon-left"></i> Product Description</a>
                                        </li>
                                    </ul>
                                    <div id="tab1dc11" class="tabs-container active">
                                        <ul>
                                            <li>
                                                plated iron spanner for use on long handle, rocker lug, or pin lug
                                                couplings from 2-1/2&quot; - 6&quot;</li>
                                            <li>
                                                open end slips over long handles for additional leverage while the claw
                                                fits rocker lugs or pin lugs, the other end can be used as a pry or
                                                crowbar</li>
                                            <li>
                                                inverted &quot;T&quot; body construction for lighter weight and improved
                                                strength</li>
                                            <li>
                                                can be used on Dixon&#39;s cam and groove pin lug or trapezoidal lug
                                                tank car adapters or 8&quot; pin lug couplings</li>
                                            <li>
                                                overall length 18-3/4&quot;</li>
                                            <li>
                                                <br />
                                                <h4>
                                                    Storz Adjustable Hydrant Wrench</h4>
                                            </li>
                                            <li>
                                                ductile iron head</li>
                                            <li>
                                                knurled chrome plated handle</li>
                                            <li>
                                                manganese bronze adjustable jaw</li>
                                        </ul>
                                        <p>&nbsp;
                                        </p>
                                        <h4>
                                            Storz Adjustable Hydrant Wrench</h4>
                                        <ul>
                                            <li>
                                                ductile iron head</li>
                                            <li>
                                                knurled chrome plated handle</li>
                                            <li>
                                                manganese bronze adjustable jaw</li>
                                        </ul>
                                        <h4>
                                            Heavy Duty Adjustable Hydrant Wrench</h4>
                                        <p>
                                        <ul>
                                            <li>
                                                plated iron head, 7/8&quot; plated steel handle</li>
                                            <li>
                                                for pentagon hydrant nuts up to 1&frac34;&quot; from point to flat</li>
                                            <li>
                                                notched to fit 1-1/4&quot; square hydrant nut</li>
                                            <li>
                                                spanner for pin lug and sensible (rocker) lug couplings, 1-1/2&quot;
                                                thru 6&quot;</li>
                                            <li>
                                                overall length 18&quot;</li>
                                        </ul>
                                        </p>
                                        <h4>
                                            Universal Wrench</h4>
                                        <ul>
                                            <li>
                                                for pin lug or sensible (rocker) lug couplings 1&quot; - 4-1/2&quot;
                                            </li>
                                            <li>
                                                a gas cock shutoff is provided in the handle</li>
                                            <li>
                                                iron, red epoxy painted</li>
                                            <li>
                                                overall length 11-3/4&quot;</li>
                                        </ul>
                                        <p>&nbsp;
                                        </p>



                                    </div>
                                    <div id="tab2dc22" class="tabs-container">
                                        <p>
                                            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
                                            inceptos himenaeos. Nam nec ante. Sed lacinia, urna non tincidunt mattis,
                                            tortor neque adipiscing diam, a cursus ipsum ante quis turpis. Nulla
                                            facilisi.
                                        </p>
                                    </div>
                                    <div id="tab3dc33" class="tabs-container">
                                        <p>
                                            Suspendisse in justo eu magna luctus suscipit. Sed lectus. Integer euismod
                                            lacus luctus magna. Quisque cursus, metus vitae pharetra auctor, sem massa
                                            mattis sem, at interdum magna augue eget diam.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Life Saving</a></li>
                                        <li><a href="firefighting">Fire Fighting</a></li>
                                        <li><a href="chemical-spill">Oil Spill Response</a></li>
                                        <li><a href="ship-deck">Ship Deck Equipments</a></li>
                                        <li><a href="respiratory-protection">Industrial Safety Equipments</a></li>
                                        <li><a href="posters-signs">Posters and Safety Signs</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>