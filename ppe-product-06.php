<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div id="product-detail">
                            <div class="row">
                                <div class="col-md-7 bottom-sm-30">
                                    <div class="product-images">
                                        <img src="assets/images/img/product_5_ppe-big.jpg" alt=""
                                            class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h2 class="product-title large thin">Ear Muffs</h2>
                                    <div class="product-review bottom-10">
                                        <span class="star-rating star-rating-4"></span>
                                    </div>

                                    <p class="top-20 bottom-20">Product Description</p>
                                    <div class="product-amount clearfix">

                                        <div class="field">
                                            <button class="button"><a href="enquiry">Enquire Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content top-40">
                                <div class="tabs main border clearfix">
                                    <ul class="tabNavigation list-unstyled bottom-0 clearfix">
                                        <li class="active"><a data-toggle="tab" href="#tab1dc11"><i
                                                    class="fa fa-align-justify icon-left"></i> Product Description</a>
                                        </li>
                                    </ul>
                                    <div id="tab1dc11" class="tabs-container active">
                                        <p>
                                            Brand : 3M</p>
                                        <table cellpadding=\"0\" cellspacing=\"0\" id=\"productDetails\" summary=\"Ear
                                            Muffs Specification\">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <strong>Attenuation Rating (SNR)</strong>
                                                    </td>
                                                    <td>
                                                        30dB<br />
                                                        31dB</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong>Protection Level</strong>
                                                    </td>
                                                    <td>
                                                        94dB(A) - 105dB(A)</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong>Typical Applications</strong>
                                                    </td>
                                                    <td>
                                                        Outdoor work<br />
                                                        Construction<br />
                                                        Airports<br />
                                                        All market segments</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong>Features</strong>
                                                    </td>
                                                    <td>
                                                        Lightweight Design<br />
                                                        Low Profile<br />
                                                        Constant pressure<br />
                                                        Liquid Filled Sealing Rings<br />
                                                        2-point attachment<br />
                                                        Available in various versions</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong>Working Environment</strong>
                                                    </td>
                                                    <td>
                                                        General Industrial Use<br />
                                                        Continuous / Long exposure to noise<br />
                                                        Dirty/dusty environment<br />
                                                        Intermittent / Short exposure to noise<br />
                                                        Presence / Handling of contaminants</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong>Approval</strong>
                                                    </td>
                                                    <td>
                                                        CE Approved</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p>&nbsp;
                                        </p>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Lifeboat/Life raft (Accessories)</a></li>
                                        <li><a href="respiratory-protection">Respiratory Protection/Public Gas
                                                Detector</a></li>
                                        <li><a href="marine-lights">Marine Lights</a></li>
                                        <li><a href="firefighting">Firefighting Equipment’s</a></li>
                                        <li><a href="respiratory-protection">Safety / PPE Items</a></li>
                                        <li><a href="valves">Valves, Fittings And Flangess</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>