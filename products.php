<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">

            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div class="row">
                            <div class="heading-area">
                                <h3 class="large bottom-0 heading-title"><span>PRODUCT</span></h3>
                                <h4 class="heading-subtitle bottom-20">Categories</h4>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                        </div>
                                        <img src="assets/images/img/dec_02_life.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="lifeboat-liferaft">LIFE SAVING</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                        </div>
                                        <img src="assets/images/img/product_1_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting">
                                                    <div class="productTitleInBox">FIRE FIGHTING</div>
                                                </a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                        </div>
                                        <img src="assets/images/img/product_1_oil1.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="chemical-spill">OIL SPILL RESPONSE</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                        </div>
                                        <img src="assets/images/img/product_01_reck.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="ship-deck">SHIP DECK EQUIPMENTS</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                        </div>
                                        <img src="assets/images/img/product_108_fire.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="respiratory-protection">INDUSTRIAL SAFETY
                                                    EQUIPMENTS</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                        </div>
                                        <img src="assets/images/img/product_112_pp.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="posters-signs">POSTERS AND SAFETY
                                                    SIGNS</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Life Saving</a></li>
                                        <li><a href="firefighting">Fire Fighting</a></li>
                                        <li><a href="chemical-spill">Oil Spill Response</a></li>
                                        <li><a href="ship-deck">Ship Deck Equipments</a></li>
                                        <li><a href="respiratory-protection">Industrial Safety Equipments</a></li>
                                        <li><a href="posters-signs">Posters and Safety Signs</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>