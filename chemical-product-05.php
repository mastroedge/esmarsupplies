<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div id="product-detail">
                            <div class="row">
                                <div class="col-md-7 bottom-sm-30">
                                    <div class="product-images">
                                        <img src="assets/images/img/product_1_oil5.jpg" alt="" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h2 class="product-title large thin">Flammable Cabinet</h2>
                                    <div class="product-review bottom-10">
                                        <span class="star-rating star-rating-4"></span>
                                    </div>

                                    <p class="top-20 bottom-20">Product Description</p>
                                    <div class="product-amount clearfix">

                                        <div class="field">
                                            <button class="button"><a href="enquiry">Enquire Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content top-40">
                                <div class="tabs main border clearfix">
                                    <ul class="tabNavigation list-unstyled bottom-0 clearfix">
                                        <li class="active"><a data-toggle="tab" href="#tab1dc11"><i
                                                    class="fa fa-align-justify icon-left"></i> Product Description</a>
                                        </li>
                                    </ul>
                                    <div id="tab1dc11" class="tabs-container active">
                                        <p>
                                            Brand : Justrite USA</p>
                                        <p>
                                            Protect workers, reduce fire risks, and improve productivity by storing
                                            flammable fuels and chemicals in code-compliant safety cabinets. Designed to
                                            meet OSHA and NFPA standards, Sure-Grip® EX cabinets are constructed of
                                            sturdy 18-gauge (1-mm) thick double-wall, welded steel with 1-1/2” (38-mm)
                                            of insulating air space for fire resistance. High-performance, self-latching
                                            doors close easily and securely for maximum protection under fire
                                            conditions. Fail-safe closing mechanism ensures three-point stainless steel
                                            bullet latching system works every time.</p>
                                        <p>
                                            For added security, U-Loc™ handle comes with a cylinder lock and key set or
                                            accepts an optional padlock. Haz-Alert™ reflective warning labels are highly
                                            visible under fire conditions or during power outages when illuminated with
                                            a flashlight.</p>
                                        <p>Patented SpillSlope® galvanized steel shelves direct spills to back and
                                            bottom of a leakproof 2” (51-mm) bottom sump. They adjust on 3” (76-mm)
                                            centers for versatile storage. Cabinets also feature dual vents with flame
                                            arresters, four adjustable self-leveling feet, grounding connector, and
                                            trilingual warning label. Durable lead-free epoxy/polyester powder-coat
                                            finish provides excellent chemical resistance.
                                        </p>
                                        <p>Manual-close door(s) opens to a full 180 degrees and self-latches when pushed
                                            closed. Self-close door(s) shuts and latches automatically when a fusible
                                            link melts at 165°F (74ºC) under fire conditions. Unique, concealed
                                            self-closing mechanism offers obstruction-free access to contents.
                                        </p>
                                        <p>All door styles meet OSHA and NFPA 30; self-close door styles also meet NFPA
                                            1 and the International Fire Code.
                                        </p>





                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Life Saving</a></li>
                                        <li><a href="firefighting">Fire Fighting</a></li>
                                        <li><a href="chemical-spill">Oil Spill Response</a></li>
                                        <li><a href="ship-deck">Ship Deck Equipments</a></li>
                                        <li><a href="respiratory-protection">Industrial Safety Equipments</a></li>
                                        <li><a href="posters-signs">Posters and Safety Signs</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>