<!DOCTYPE html>
<html lang="en">

<head>
    <title>Best range of Calibration / Trouble Shooting Servicing available in Abu Dhabi, Dubai, Fujairah | UAE</title>
    <meta name="description"
        content="Get the best Calibration / Trouble Shooting in UAE. We have expert technicians in Dubai to inspect & service.">
    <meta name="author" content="">

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8 bottom-sm-30">
                        <div class="entry-item">
                            <div class="entry-header">

                                <div class="main-heading text-left">
                                    <h2 class="heading-title wow fadeInDown" data-wow-duration="0.6s"
                                        data-wow-delay="0.6s">Calibration / Electrical / Automation Jobs</h2>
                                    <h3><span class="text-orange">(Troubleshoot / Repair / Maintenance /
                                            Services)</span></h3>
                                </div>
                                <div class="entry-media bottom-20">
                                    <a href="#"><img src="assets/images/img/new_img/service-05.jpg"
                                            class="img-responsive" alt=""></a>
                                </div>
                                <div class="entry-content">
                                    <h5> Below we have listed few key jobs we handle.</h5>
                                    <ul class="listing-normal">
                                        <li>Portable Single & Multigas Detectors Service and Calibration</li>
                                        <li>PLC Programming & Modification (Familiar with ABB & SIEMENS LOGO)</li>
                                        <li>Main Switchboard Safety System Troubleshooting (MSB)</li>
                                        <li>Synchronization & Load Sharing of Auxiliary Engines.</li>
                                        <li>Installation, commissioning & Maintenance of VFD and Soft Starters</li>
                                        <li>Calibration of AVR (Automatic Voltage Regulation)</li>
                                        <li>Gas Detection/Monitoring Systems</li>
                                        <li>Tank Gas Monitoring Systems</li>
                                        <li>Generator and Main Engine Shut Down Systems</li>
                                        <li>Troubleshooting & Testing of Electrical Control Panels</li>
                                        <li>Maintenance of Electrical control system for WINCH System and SHARK JAW
                                            System</li>
                                        <li>MEGGER Testing</li>
                                        <li>Testing of Air Circuit Breakers (ACB) & Molded Case Circuit Breakers (MCCB)
                                        </li>
                                        <li>Bridge Navigation Watch Alarm System</li>
                                        <li>Trouble Shooting of Bow Thruster and Steering System</li>
                                        <li>Electrical Maintenance of Galley Utensils</li>
                                        <li>CCTV Installation and Maintenance </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Services</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="liferaft-service">Liferaft Service</a></li>
                                        <li><a href="life-saving-appliances-service">Life Saving Appliances Service</a>
                                        </li>
                                        <li><a href="fire-fighting-service">Fire Fighting Appliances Service</a></li>
                                        <li><a href="lifeboat-rescue-service">Lifeboat / Rescue Boat Service</a></li>
                                        <li class="active"><a href="trouble-shooting-service">Calibration / Trouble
                                                Shooting</a></li>
                                        <li><a href="hydraulic-fittings">Hydraulic and Pneumatic – Hoses And
                                                Fittings.</a></li>
                                    </ul>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>