<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>
        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div class="row">
                            <div class="heading-area">
                                <h3 class="bottom-0 heading-title wow fadeInDown" data-wow-duration="0.6s"
                                    data-wow-delay="0.6s">
                                    Industrial Safety Equipments</h3>

                                <h4 class="heading-subtitle bottom-20">.</h4>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a
                                                        href="respiratory-product-01">Quick view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_iq_01s.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="respiratory-product-01">Hand operated
                                                    siren</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a
                                                        href="respiratory-product-02">Quick view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_iq_02s.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="respiratory-product-02">Electrical
                                                    siren</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a
                                                        href="respiratory-product-03">Quick view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_iq_03s.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="respiratory-product-03">Air Horn</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a
                                                        href="respiratory-product-04">Quick view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_108_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="respiratory-product-04">Single Gas
                                                    Monitors</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a
                                                        href="respiratory-product-05">Quick view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_4_resp.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="respiratory-product-05">Oxygen Gas
                                                    Monitors</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a
                                                        href="respiratory-product-07">Quick view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_116_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="respiratory-product-07">Escape Hood</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a
                                                        href="respiratory-product-08">Quick view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_iq_05s.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="respiratory-product-08">Mega phone</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a
                                                        href="respiratory-product-09">Quick view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_iq_04s.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="respiratory-product-09">Wind Sock</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-08">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_801_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-08">
                                                    <div class="productTitleInBox">Alcohol Tester</div>
                                                </a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Lifeboat/Life raft (Accessories)</a></li>
                                        <li><a href="respiratory-protection">Respiratory Protection/Public Gas
                                                Detector</a></li>
                                        <li><a href="marine-lights">Marine Lights</a></li>
                                        <li><a href="firefighting">Firefighting Equipment’s</a></li>
                                        <li><a href="respiratory-protection">Safety / PPE Items</a></li>
                                        <li><a href="valves">Valves, Fittings And Flangess</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>


</body>

</html>