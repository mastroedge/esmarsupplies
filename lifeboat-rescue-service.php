<!DOCTYPE html>
<html lang="en">

<head>
    <title>Best range of LIFEBOATS or RESCUE BOATS Servicing and Davits Winch System Inspection available in Abu Dhabi,
        Dubai, Fujairah | ESMAR UAE</title>
    <meta name="description"
        content="Get the best Lifeboat,  Rescue Boat Service in UAE. We have expert technicians in Dubai to Davits Winch System Inspection and service.">
    <meta name="author" content="">

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8 bottom-sm-30">
                        <div class="entry-item">
                            <div class="entry-header">

                                <div class="main-heading text-left">
                                    <h2 class="heading-title wow fadeInDown" data-wow-duration="0.6s"
                                        data-wow-delay="0.6s">Lifeboats / Rescue Boats / Davits <span
                                            class="text-orange"> Winch System Inspection</span></h2>
                                </div>
                                <div class="entry-media bottom-20">
                                    <a href="#"><img src="assets/images/img/new_img/service-04.jpg"
                                            class="img-responsive" alt=""></a>
                                </div>
                                <div class="entry-content">
                                    <p>Our Service Engineers have manufacturer approval to carryout Lifeboat/Rescue Boat
                                        Inspection, Service & Certification. According to IMO Circular MSC/Circ. 1206 we
                                        can carry out services under class inspector supervision and Flag approval.</p>
                                    <ul class="listing-normal">
                                        <li>Annual Inspection of Lifeboats/Rescue Boats and Davits as per latest IMO
                                            regulations.</li>
                                        <li>Annual service approved by lifeboat/rescue boat and davit makers or service
                                            under flag register with class supervision. 5 yearly test including load
                                            test of lifeboats and davits. Re-hooking of lifeboats and davits.</li>

                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Services</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="liferaft-service">Liferaft Service</a></li>
                                        <li><a href="life-saving-appliances-service">Life Saving Appliances Service</a>
                                        </li>
                                        <li><a href="fire-fighting-service">Fire Fighting Appliances Service</a></li>
                                        <li class="active"><a href="lifeboat-rescue-service">Lifeboat / Rescue Boat
                                                Service</a></li>
                                        <li><a href="trouble-shooting-service">Calibration / Trouble Shooting</a></li>
                                        <li><a href="hydraulic-fittings">Hydraulic and Pneumatic – Hoses And
                                                Fittings.</a></li>
                                    </ul>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>