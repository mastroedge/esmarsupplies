<!DOCTYPE html>
<html lang="en">

<head>
    <title>Best LIFERAFTS Servicing and Repairing Company in Abu Dhabi, Dubai, Fujairah Ports UAE</title>
    <meta name="description"
        content="The Best Liferaft Repairing and Servicing in accordance to IMO and SOLAS regulations.Esmar offers Annual inspection and re-certification.Our servive stations are well equipped for all kinds of repair.We have presence is all major Ports in UAE - Abu Dhabi, Dubai, Fujairah">
    <meta name="author" content="">

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8 bottom-sm-30">
                        <div class="entry-item">
                            <div class="entry-header">

                                <div class="main-heading text-left">
                                    <h2 class="heading-title wow fadeInDown" data-wow-duration="0.6s"
                                        data-wow-delay="0.6s">LIFERAFTS <span class="text-orange">SERVICE</span></h2>
                                </div>
                                <div class="entry-media bottom-20">
                                    <a href="#"><img src="assets/images/img/new_img/service-01.jpg"
                                            class="img-responsive" alt=""></a>
                                </div>
                                <div class="entry-content">
                                    <p>Liferaft is one of the most important pieces of safety equipment on a vessel.
                                        Annual Servicing will ensure that the liferaft will operate and perform as
                                        designed. Each year the liferaft is subject to a wide range of elements which
                                        will accelerate the natural ageing of the rafts and its components. With annual
                                        inspection and servicing
                                        we can increase the life expectancy of the raft.</p>
                                    <p>ESMAR have qualified engineers and skilled technicians who are trained to service
                                        Liferafts in accordance to IMO and SOLAS regulations. The raft is pressure
                                        tested and a number of checks are completed to ensure the raft complies. The
                                        internal pack is checked and items changed where necessery. Our service stations
                                        is well equipped for Inspection, Service and Certification of the below
                                        Liferafts.</p>
                                    <p>ESMAR has built up a reputation of being a reliable and efficient
                                        Lifeboat/Liferaft service provider to vessels calling UAE ports. </p>
                                    <ul class="listing-normal">
                                        <li>Annual inspection and re-certification of Liferafts</li>
                                        <li>Service approved by all Liferaft makers
                                            (CSM, Youlong, SDR Dacheng, Haining, Duarry, Arimar, CRV, SEA AIR, HYF,
                                            Fengfan (Eversailing) CUNHONG and TOKUGAWA).</li>

                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Services</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li class="active"><a href="liferaft-service">Liferaft Service</a></li>
                                        <li><a href="life-saving-appliances-service">Life Saving Appliances Service</a>
                                        </li>
                                        <li><a href="fire-fighting-service">Fire Fighting Appliances Service</a></li>
                                        <li><a href="lifeboat-rescue-service">Lifeboat / Rescue Boat Service</a></li>
                                        <li><a href="trouble-shooting-service">Calibration / Trouble Shooting</a></li>
                                        <li><a href="hydraulic-fittings">Hydraulic and Pneumatic – Hoses And
                                                Fittings.</a></li>
                                    </ul>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>