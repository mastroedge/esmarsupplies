<!DOCTYPE html>
<html lang="en">

<head>
    <title>Best FIRE FIGHTING APPLIANCES Servicing Company in UAE - Esmar Abu Dhabi, Dubai, Sharjah, Fujairah Ports
    </title>
    <meta name="description"
        content="Get the best Fire Fighting Appliances Service in UAE. Fire Extinguishers’ Annual Inspection, CO2 System Visual Inspection of control valves blow air through, Foam Sample Analysis, Alarm System Annual Inspection & Certification, Novec System Annual Inspection etc">
    <meta name="author" content="">
    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8 bottom-sm-30">
                        <div class="entry-item">
                            <div class="entry-header">

                                <div class="main-heading text-left">
                                    <h2 class="heading-title wow fadeInDown" data-wow-duration="0.6s"
                                        data-wow-delay="0.6s">FIRE FIGHTING <span class="text-orange">APPLIANCES</span>
                                    </h2>
                                </div>
                                <div class="entry-media bottom-20">
                                    <a href="#"><img src="assets/images/img/new_img/service-03.jpg"
                                            class="img-responsive" alt=""></a>
                                </div>
                                <div class="entry-content">
                                    <p>Fire protection systems and appliances should be serviced, examined and tested
                                        periodically.</p>
                                    <p>ESMAR have qualified engineers and skilled technicians who are trained to service
                                        all Fire Fighting equipment’s in accordance to IMO and SOLAS regulations. Our
                                        service stations is well equipped for Inspection, Service and Certification of
                                        the below equipment’s.</p>
                                    <h5>Fire Fighting Services</h5>
                                    <ul class="listing-normal">
                                        <li>Fire Extinguishers’ Annual Inspection / Refilling / Hydrotest
                                            <ul>
                                                <li>Portable Fire Extinguishers Services</li>
                                                <li>Non-Portable Fire Extinguishers Services</li>
                                            </ul>
                                        </li>
                                        <li>Fixed Fire Extinguishing Systems’ Inspection (CO2 system, Water mist system,
                                            Galley CO2 system, Fixed foam system, Wet chemical system). </li>
                                        <li>CO2 System Visual Inspection of control valves blow air through. Two yearly
                                            inspection includes level check of all cylinders. Five yearly inspection:
                                            internal inspection of control valves. Ten yearly inspection is cylinder
                                            hydrostatic pressure test and internal inspection (min 10%)</li>
                                        <li>Foam Sample Analysis</li>
                                        <li>FM200 / Novec System Annual Inspection / Refilling / Hydrotest /
                                            Certification.</li>
                                        <li>Fixed Fire Detection / Alarm System Annual Inspection & Certification.</li>
                                        <li>Sprinkler System</li>

                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Services</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="liferaft-service">Liferaft Service</a></li>
                                        <li><a href="life-saving-appliances-service">Life Saving Appliances Service</a>
                                        </li>
                                        <li class="active"><a href="fire-fighting-service">Fire Fighting Appliances
                                                Service</a></li>
                                        <li><a href="lifeboat-rescue-service">Lifeboat / Rescue Boat Service</a></li>
                                        <li><a href="trouble-shooting-service">Calibration / Trouble Shooting</a></li>
                                        <li><a href="hydraulic-fittings">Hydraulic and Pneumatic – Hoses And
                                                Fittings.</a></li>
                                    </ul>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>