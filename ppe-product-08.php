<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div id="product-detail">
                            <div class="row">
                                <div class="col-md-7 bottom-sm-30">
                                    <div class="product-images">
                                        <img src="assets/images/img/product_5_ppe-big.jpg" alt=""
                                            class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h2 class="product-title large thin">Safety Helmets</h2>
                                    <div class="product-review bottom-10">
                                        <span class="star-rating star-rating-4"></span>
                                    </div>

                                    <p class="top-20 bottom-20">Product Description</p>
                                    <div class="product-amount clearfix">

                                        <div class="field">
                                            <button class="button"><a href="enquiry">Enquire Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content top-40">
                                <div class="tabs main border clearfix">
                                    <ul class="tabNavigation list-unstyled bottom-0 clearfix">
                                        <li class="active"><a data-toggle="tab" href="#tab1dc11"><i
                                                    class="fa fa-align-justify icon-left"></i> Product Description</a>
                                        </li>
                                    </ul>
                                    <div id="tab1dc11" class="tabs-container active">
                                        <p>
                                            Brand : MSA / NORTH / 3M</p>
                                        <p>&nbsp;
                                        </p>
                                        <ul>
                                            <li>
                                                Robust design, durable shell construction</li>
                                            <li>
                                                Cooling vents for improved air circulation and comfort</li>
                                            <li>
                                                Glaregard&reg; Surface underbrim to reduce reflective glare</li>
                                            <li>
                                                Lateral contours for easy integration of hearing protection</li>
                                            <li>
                                                Extended rain trough for around-the-brim protection in hot and/or wet
                                                environments</li>
                                            <li>
                                                Available in a variety of colors</li>
                                            <li>
                                                Custom imprinting and striping options</li>
                                        </ul>
                                        <ul>
                                            <li>
                                                Hardhats with reverse donning logo may be worn backwards without voiding
                                                approvals</li>
                                            <li>
                                                Sizes: standard 6-1/2&rdquo; to 8&rdquo;</li>
                                            <li>
                                                Available colors: white, blue, yellow, red, green, Hi-Viz&trade; Orange,
                                                Hi-Viz&trade; Yellow-Green</li>
                                            <li>
                                                Also available in vented</li>
                                            <li>
                                                Replacement suspension: 4-Point or 6-Point Fas-Trac&reg;</li>
                                            <li>
                                                Custom imprinting and striping available</li>
                                            <li>
                                                USA Approvalz</li>
                                            <li>
                                                ANSI/ISEA Z89.1-2009 (Class C, Type I)</li>
                                            <li>
                                                AS/NZS</li>
                                            <li>
                                                Europe
                                                <ul>
                                                    <li>
                                                        certified according to EN 397, with the following optional
                                                        requirements:<br />
                                                        - very low temperature [-30 &deg;C]<br />
                                                        - lateral deformation [LD]<br />
                                                        - electrical insulation [non vented version only - 440 V
                                                        AC]<br />
                                                        - molten metal splash [MM]</li>
                                                    <li>
                                                        &nbsp;approved to the following standards:<br />
                                                        - EN 50365 - Electrically insulating helmets for low voltage
                                                        installations<br />
                                                        - EN 13463-1 - Use in Potentially Explosive Atmosphere [ATEX]
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                Russia: GOST with optional requirements [-50&deg;C / +90&deg;C]</li>
                                        </ul>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Lifeboat/Life raft (Accessories)</a></li>
                                        <li><a href="respiratory-protection">Respiratory Protection/Public Gas
                                                Detector</a></li>
                                        <li><a href="marine-lights">Marine Lights</a></li>
                                        <li><a href="firefighting">Firefighting Equipment’s</a></li>
                                        <li><a href="respiratory-protection">Safety / PPE Items</a></li>
                                        <li><a href="valves">Valves, Fittings And Flangess</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>