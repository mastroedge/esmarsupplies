<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div id="product-detail">
                            <div class="row">
                                <div class="col-md-7 bottom-sm-30">
                                    <div class="product-images">
                                        <img src="assets/images/img/product_102_a.jpg" alt="" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h2 class="product-title large thin">
                                        Retro Reflective Tape - Solas
                                    </h2>
                                    <div class="product-review bottom-10">
                                        <span class="star-rating star-rating-4"></span>
                                    </div>

                                    <div class="product-amount clearfix">

                                        <div class="field">
                                            <button class="button"><a href="enquiry">Enquire Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content top-40">
                                <div class="tabs main border clearfix">
                                    <ul class="tabNavigation list-unstyled bottom-0 clearfix">
                                        <li class="active"><a data-toggle="tab" href="#tab1dc11"><i
                                                    class="fa fa-align-justify icon-left"></i> Product Description</a>
                                        </li>
                                    </ul>
                                    <div id="tab1dc11" class="tabs-container active">
                                        <p>
                                            <strong>Brand: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; T-ISS,
                                                Netherlands</strong>
                                        </p>
                                        <p>
                                            <strong>Part
                                                no:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TM-3150</strong><br />
                                            <br />
                                            T-ISS retro reflective tape is intended for reflectorizing SOLAS life suport
                                            equipment such as life vests, jackets, and rafts. It conforms to, IMO Res.
                                            A.658(16) and meets U.S. Coast Guard specification 46 CFR Part 164, Subpart
                                            164.018/5/0. It is approved by numerous national authorities around the
                                            globe. It is approved with the Wheelmark approval by Bureau Veritas.<br />
                                            <br />
                                            <br />
                                            <strong>Retro reflective Performance:</strong><br />
                                            The coefficient of retroreflection (RA, in cd/lux/m2) is measured by methods
                                            traceable to either of the following retroreflective<br />
                                            intensity testing procedures:<br />
                                            ASTM E809 and E810 (RA)<br />
                                            CIE 54: 1982 (R&rsquo;)<br />
                                            <br />
                                            Based on tests performed by SOLASTAPE it is in accordance with IMO
                                            procedures and verified by an outside third party, T-ISS SOLASFLEX
                                            reflective material meets or exceed these values.<br />
                                            <br />
                                            <br />
                                            <strong>Packaging &amp; Other info:</strong><br />
                                            Box each 1,2 kilo.<br />
                                            Transportboxes boxes of 12 (14kg) each or 21 each (31kg)<br />
                                            Tarifcode 3919906999<br />
                                            ISSA code 4700704 SILVER 50MM<br />
                                            IMPA code 330189 SILVER :50MM
                                        </p>





                                        <p><strong>Certificates :</strong> Information not available</p>
                                    </div>
                                    <div id="tab2dc22" class="tabs-container">
                                        <p>
                                            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
                                            inceptos himenaeos. Nam nec ante. Sed lacinia, urna non tincidunt mattis,
                                            tortor neque adipiscing diam, a cursus ipsum ante quis turpis. Nulla
                                            facilisi.
                                        </p>
                                    </div>
                                    <div id="tab3dc33" class="tabs-container">
                                        <p>
                                            Suspendisse in justo eu magna luctus suscipit. Sed lectus. Integer euismod
                                            lacus luctus magna. Quisque cursus, metus vitae pharetra auctor, sem massa
                                            mattis sem, at interdum magna augue eget diam.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Life Saving</a></li>
                                        <li><a href="firefighting">Fire Fighting</a></li>
                                        <li><a href="chemical-spill">Oil Spill Response</a></li>
                                        <li><a href="ship-deck">Ship Deck Equipments</a></li>
                                        <li><a href="respiratory-protection">Industrial Safety Equipments</a></li>
                                        <li><a href="posters-signs">Posters and Safety Signs</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>