<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div id="product-detail">
                            <div class="row">
                                <div class="col-md-7 bottom-sm-30">
                                    <div class="product-images">
                                        <img src="assets/images/img/product_1_fire-big.jpg" alt=""
                                            class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h2 class="product-title large thin">Fire Hoses - Inner / Outer</h2>
                                    <div class="product-review bottom-10">
                                        <span class="star-rating star-rating-4"></span>
                                    </div>

                                    <p class="top-20 bottom-20">Product Description</p>
                                    <div class="product-amount clearfix">

                                        <div class="field">
                                            <button class="button"><a href="enquiry">Enquire Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content top-40">
                                <div class="tabs main border clearfix">
                                    <ul class="tabNavigation list-unstyled bottom-0 clearfix">
                                        <li class="active"><a data-toggle="tab" href="#tab1dc11"><i
                                                    class="fa fa-align-justify icon-left"></i> Product Description</a>
                                        </li>
                                    </ul>
                                    <div id="tab1dc11" class="tabs-container active">
                                        <p>
                                            Brand: OSW Germany / MAGMEX India</p>
                                        <p>
                                            <strong>JACKET: </strong>100% high tenacity polyester yarn, circular-woven
                                            twill weave, Warp threads 2-ply twisted, max. change in diameter &lt; 5%,
                                            max. change in length &lt; 5%
                                        </p>
                                        <p>
                                            <strong>PROPERTIES: </strong>seawater-resistant, weather-resistant, ageing
                                            and ozone resistant,light and flexible, small coil diameter, temperature
                                            range from -40?up to +100?
                                        </p>
                                        <p>
                                            <strong>INNER Lining: </strong>high-quality, very light synthetic rubber on
                                            the basis of EPDM, maximum lengths up to 500 m
                                        </p>
                                        <p>
                                            <strong>EXTERNAL COATING: </strong>synthetic coating of polyurethane for
                                            better,abrasion and protection,flame-resistant,oil-and fuel-resistant
                                        </p>
                                        <p>
                                            standardcolour: red, other on request</p>
                                        <p>
                                            <strong>APPROVALS</strong>: DIN 14811:2008class 1/class 2, BS 6391 Type
                                            1(white), BS 6391 Type 2 (red coated), M.E.D. 96/98/EC
                                        </p>
                                        <p>
                                            <strong>Inner Diameter:</strong> 1&quot; 1.5&quot; 2&#39; 2.5&#39; 3&#39;
                                            3.5&#39; 4&#39;
                                        </p>
                                        <p>&nbsp;
                                        </p>



                                        <p><strong>Certificates :</strong> Information not available</p>
                                    </div>
                                    <div id="tab2dc22" class="tabs-container">
                                        <p>
                                            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
                                            inceptos himenaeos. Nam nec ante. Sed lacinia, urna non tincidunt mattis,
                                            tortor neque adipiscing diam, a cursus ipsum ante quis turpis. Nulla
                                            facilisi.
                                        </p>
                                    </div>
                                    <div id="tab3dc33" class="tabs-container">
                                        <p>
                                            Suspendisse in justo eu magna luctus suscipit. Sed lectus. Integer euismod
                                            lacus luctus magna. Quisque cursus, metus vitae pharetra auctor, sem massa
                                            mattis sem, at interdum magna augue eget diam.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Life Saving</a></li>
                                        <li><a href="firefighting">Fire Fighting</a></li>
                                        <li><a href="chemical-spill">Oil Spill Response</a></li>
                                        <li><a href="ship-deck">Ship Deck Equipments</a></li>
                                        <li><a href="respiratory-protection">Industrial Safety Equipments</a></li>
                                        <li><a href="posters-signs">Posters and Safety Signs</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>