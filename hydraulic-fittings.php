<!DOCTYPE html>
<html lang="en">

<head>
    <title>Best range of Hydraulic and Pneumatic Service available in Abu Dhabi, Dubai, Fujairah | UAE</title>
    <meta name="description"
        content="Get the best Hydraulic and Pneumatic – Hoses And Fittings in UAE. We have expert technicians in Dubai to inspect & service.">
    <meta name="author" content="">

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8 bottom-sm-30">
                        <div class="entry-item">
                            <div class="entry-header">


                                <div class="main-heading text-left">
                                    <h2 class="heading-title wow fadeInDown" data-wow-duration="0.6s"
                                        data-wow-delay="0.6s">HYDRAULIC AND
                                        PNEUMATIC <span class="text-orange"> HOSES AND FITTINGS</span></h2>
                                </div>
                                <div class="entry-media bottom-20">
                                    <a href="#"><img src="assets/images/img/new_img/service-06.jpg"
                                            class="img-responsive" alt=""></a>
                                </div>
                                <div class="entry-content">
                                    <p>We are pleased to introduce our new division of Hydraulic and Pneumatic – pipes
                                        and fittings. With
                                        direct imports from Italy, Germany, China and Malaysia, rest assured our prices
                                        will be unmatched
                                        with all items and sizes available in stock.</p>
                                    <p>We welcome all inquiries and queries regarding the same and guarantee you most
                                        cost-effective
                                        solutions.</p>
                                    <h5> Below is the summary of our new services and products provided:</h5>
                                    <ol class="listing-normal">
                                        <li>Hydraulic Hoses Crimping for all types of hoses from 1/8" to 12"</li>
                                        <li>Mobile services available 24/7 </li>
                                        <li>SS hose assembly, welding and crimping</li>
                                        <li>In house lathe machine and milling facilities </li>
                                        <li>Ex-stock of all types of Hoses – Composite / Chemical / Oil Suction Delivery
                                            / Water suction
                                            delivery / Air hoses / Steam hoses – in all sizes</li>
                                        <li>PVC Duct hoses and Reinforced hoses for both High and low pressure</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Services</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="liferaft-service">Liferaft Service</a></li>
                                        <li><a href="life-saving-appliances-service">Life Saving Appliances Service</a>
                                        </li>
                                        <li><a href="fire-fighting-service">Fire Fighting Appliances Service</a></li>
                                        <li><a href="lifeboat-rescue-service">Lifeboat / Rescue Boat Service</a></li>
                                        <li><a href="trouble-shooting-service">Calibration / Trouble Shooting</a></li>
                                        <li class="active"><a href="hydraulic-fittings">Hydraulic and Pneumatic – Hoses
                                                And Fittings.</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="service-onsite">
                                <img alt="" src="./assets/images/img/new_img/service-06-1.jpg" style="width:100%;">
                                <span class="service-time">
                                    <img alt="" src="./assets/images/img/new_img/service-06-2.jpg">
                                </span>
                                <div class="text-center">
                                    <h2 class="heading-title wow fadeInDown service-type" data-wow-duration="0.6s"
                                        data-wow-delay="0.6s">
                                        ONSITE HOSE REPAIR<span class="text-orange"> SERVICES</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>



</body>

</html>