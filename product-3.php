<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div id="product-detail">
                            <div class="row">
                                <div class="col-md-7 bottom-sm-30">
                                    <div class="product-images">
                                        <img src="assets/images/img/product_03_a.jpg" alt="" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h2 class="product-title large thin">
                                        Hydrostatic Release Unit
                                    </h2>
                                    <div class="product-review bottom-10">
                                        <span class="star-rating star-rating-4"></span>
                                    </div>

                                    <div class="product-amount clearfix">

                                        <div class="field">
                                            <button class="button"><a href="enquiry">Enquire Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content top-40">
                                <div class="tabs main border clearfix">
                                    <ul class="tabNavigation list-unstyled bottom-0 clearfix">
                                        <li class="active"><a data-toggle="tab" href="#tab1dc11"><i
                                                    class="fa fa-align-justify icon-left"></i> Product Description</a>
                                        </li>
                                    </ul>
                                    <div id="tab1dc11" class="tabs-container active">

                                        <h1>
                                            <span style=\"font-size:16px;\">HAMMER H20 Solas Model</span>
                                        </h1>
                                        <p>
                                            <span style=\"font-family:times new roman,times,serif;\"><span
                                                    style=\"font-size: 12px;\"><strong>The Hammar H20 hydrostatic
                                                        release unit is designed for liferafts from 6 up to 150
                                                        persons.</strong><br />
                                                    <br />
                                                    The Hammar H20 release unit consists of a double looped white rope
                                                    line, </span></span>
                                        </p>
                                        <p>
                                            <span style=\"font-family:times new roman,times,serif;\"><span
                                                    style=\"font-size: 12px;\">(breaking strength 2,2 &plusmn;
                                                    0,4kN).<br />
                                                    <br />
                                                    The white strong rope of Hammar H20 is secured to the deck or
                                                    liferaft cradle and attached to the liferaft lashing with a
                                                    sliphook. If the ship sinks, the water pressure will, within 4
                                                    metres, activate the sharp knife which cuts the white strong rope
                                                    and the liferaft will float free. As the ship sinks, the liferaft
                                                    painter line will be stretched and the liferaft starts to inflate.
                                                    The Red Weak Link&trade; breaks and survivors can board the floating
                                                    liferaft.</span></span>
                                        </p>
                                        <p>
                                            <br />
                                            <span style=\"font-family:times new roman,times,serif;\"><span
                                                    style=\"font-size: 12px;\"><strong>Approvals</strong><br />
                                                    The Hammar H20 hydrostatic release units are approved all over the
                                                    world, fulfil the requirements of the SOLAS 74/96 Convention, Reg.
                                                    III/4 LSA Code, MO Res. MSC. 48/(66), IMO Res. MSC. 81(70), approved
                                                    to EU Directive 96/98 EC on Marine Equipment and has a Nato stock
                                                    number. Release depth 1,5 &ndash; 4,0&nbsp; metres according to
                                                    IMO/SOLAS regulations.</span></span>
                                        </p>




                                        <p><strong>Certificates :</strong> Information not available</p>
                                    </div>
                                    <div id="tab2dc22" class="tabs-container">
                                        <p>
                                            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
                                            inceptos himenaeos. Nam nec ante. Sed lacinia, urna non tincidunt mattis,
                                            tortor neque adipiscing diam, a cursus ipsum ante quis turpis. Nulla
                                            facilisi.
                                        </p>
                                    </div>
                                    <div id="tab3dc33" class="tabs-container">
                                        <p>
                                            Suspendisse in justo eu magna luctus suscipit. Sed lectus. Integer euismod
                                            lacus luctus magna. Quisque cursus, metus vitae pharetra auctor, sem massa
                                            mattis sem, at interdum magna augue eget diam.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Life Saving</a></li>
                                        <li><a href="firefighting">Fire Fighting</a></li>
                                        <li><a href="chemical-spill">Oil Spill Response</a></li>
                                        <li><a href="ship-deck">Ship Deck Equipments</a></li>
                                        <li><a href="respiratory-protection">Industrial Safety Equipments</a></li>
                                        <li><a href="posters-signs">Posters and Safety Signs</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>