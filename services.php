<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description"
        content="ESMAR is an approved Liferaft and Lifeboat servicing as well as Life saving and Fire Fighting Appliances servicing Company in Dubai, Abu Dhabi and Fujairah ports in UAE">
    <link rel="canonical" href="https://esmarsupplies.com/services" />
    <meta name="author" content="">
    <title>Liferaft and Lifeboat Servicing | Life saving and Fire Fighting Appliances Servicing in Dubai, Abu Dhabi,
        Fujairah Ports UAE</title>


    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8 bottom-sm-30">
                        <div class="entry-item">
                            <div class="entry-header">

                                <div class="heading-area">
                                    <h3 class="large bottom-0 heading-title"><span>SERVICES</span></h3>
                                    <h4 class="heading-subtitle bottom-20">We are dedicated to achieving excellence in
                                        our work</h4>
                                </div>
                                <div class="entry-media bottom-20" style="float:left;">
                                    <a href="#"><img src="assets/images/img/serv-001.jpg" class="img-responsive"
                                            alt=""></a>
                                </div>
                                <div class="entry-media bottom-20" style="float:left;">
                                    <a href="#"><img src="assets/images/img/serv-002.jpg" class="img-responsive"
                                            alt=""></a>
                                </div>

                                <div class="entry-media bottom-20">
                                    <a href="#"><img src="assets/images/img/serv-003.jpg" class="img-responsive"
                                            alt=""></a>
                                </div>

                                <div class="entry-media bottom-20" style="float:left;">
                                    <a href="#"><img src="assets/images/img/serv-004.jpg" class="img-responsive"
                                            alt=""></a>
                                </div>
                                <div class="entry-media bottom-20" style="float:left;">
                                    <a href="#"><img src="assets/images/img/serv-005.jpg" class="img-responsive"
                                            alt=""></a>
                                </div>

                                <div class="entry-media bottom-20">
                                    <a href="#"><img src="assets/images/img/serv-006.jpg" class="img-responsive"
                                            alt=""></a>
                                </div>
                                <br>

                                <div class="entry-content">
                                    We at ESMAR provide service of Fire Fighting and Lifesaving Equipments for both -
                                    onshore and offshore
                                    facilities. Our service stations is well equipped for inspection, Service and
                                    Certification of life
                                    raft, Fire extinguishers, Breathing Aparatus, Emergency Escape Breathing Devices and
                                    Hydro-testing of
                                    High Pressure and low pressure Cylinders.</p>
                                    <p>ESMAR has qualified engineers and skilled technicians who are trained to service
                                        all Fire Fighting
                                        and Life saving equipments in accordance to international standards and
                                        regulations.</p>
                                    <p style=" color:#fea100"><strong>FIRE FIGHTING EQUIPMENTS</strong></p>

                                    <p>
                                        Fire Extinguisher All types Portable & Non Portable<br>
                                        Fixed CO2 System<br>
                                        FM 200& Novec 1230<br>
                                        Spinkler/ Water Mist System<br>
                                        Fire Detection System<br>
                                    </p>

                                    <p style=" color:#fea100"><strong>LIFE SAVING EQUIPMENTS</strong></p>

                                    <p>
                                        Life Rafts<br>
                                        Breathing Apparatus<br>
                                        EEBD's<br>
                                        Inflatable Life Jackets<br>
                                        Immersion Suits<br>
                                    </p>

                                    <p>Immersion Suits

                                        Apart from the above, with our strong distributorship model and vast inventory
                                        for various marine
                                        approved equipment, we can offer our clients with the best prices for all types
                                        of Life Saving and
                                        Fire Fighting Equipments.</p>
                                </div>
                                <div class="entry-meta text-center top-30">
                                    <div class="row"></div>
                                </div>

                                <div class="entry-relate text-center">
                                    <div class="row"></div>
                                </div>



                            </div>
                        </div>
                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Services</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="liferaft-service">Liferaft Service</a></li>
                                        <li><a href="life-saving-appliances-service">Life Saving Appliances Service</a>
                                        </li>
                                        <li><a href="fire-fighting-service">Fire Fighting Appliances Service</a></li>
                                        <li><a href="lifeboat-rescue-service">Lifeboat / Rescue Boat Service</a></li>
                                        <li><a href="trouble-shooting-service">Calibration / Trouble Shooting</a></li>
                                        <li><a href="hydraulic-fittings">Hydraulic and Pneumatic – Hoses And
                                                Fittings.</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>



</body>

</html>