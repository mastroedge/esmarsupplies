<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>
    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>
        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div id="product-detail">
                            <div class="row">
                                <div class="col-md-7 bottom-sm-30">
                                    <div class="product-images"> <img src="assets/images/img/product_1_oil1.jpg" alt=""
                                            class="img-responsive"> </div>
                                </div>
                                <div class="col-md-5">
                                    <h2 class="product-title large thin">Absorbent Pads</h2>
                                    <div class="product-review bottom-10"> <span
                                            class="star-rating star-rating-4"></span> </div>
                                    <p class="top-20 bottom-20">Product Description</p>
                                    <div class="product-amount clearfix">
                                        <div class="field">
                                            <button class="button"><a href="enquiry">Enquire Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content top-40">
                                <div class="tabs main border clearfix">
                                    <ul class="tabNavigation list-unstyled bottom-0 clearfix">
                                        <li class="active"><a data-toggle="tab" href="#tab1dc11"><i
                                                    class="fa fa-align-justify icon-left"></i> Product Description</a>
                                        </li>
                                    </ul>
                                    <div id="tab1dc11" class="tabs-container active">
                                        <p> Absorbent Pads are a great way to control fluids spilled in both indoor and
                                            outdoor environments. Do you have spills that require these types of product
                                            for these conditions? Do you want to eliminate them for good without having
                                            to hit your workers with a rolled up newspaper. A clean and efficient
                                            workplace uses Absorbent Pads that will effetively maintain a clean work
                                            environment.</p>
                                        <p> Eliminate those messy drips, leaks and overspray of fluids with products
                                            like our Universal Pads that soak up spill fast. Other Absorbents include
                                            Rolls, Socks and more, at chewed to the bone prices. Remember, we offer a
                                            price guarantee on all our Absorbents. If you find an Absorbent Pad or any
                                            other type of Absorbent for less money we will give you a 5% reduction from
                                            the lowest price you found. </p>
                                        <p> • An excellent sock at an economical price</p>
                                        <p>• Corncob-filled Socks come packed with plenty of absorbing power so you buy
                                            fewer socks and enjoy reduced disposal costs </p>
                                        <p>• Absorbent socks ideal for oil spill control around machine bases, spill
                                            containment, and spill cleanup </p>
                                        <p>• DAWG120 comes with ties so you can cut the length you need from the 160'L
                                            sock and tie it off with nylon ties (ties included at no extra charge) </p>
                                        <p>• choose from 3 lengths of absorbent socks </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Life Saving</a></li>
                                        <li><a href="firefighting">Fire Fighting</a></li>
                                        <li><a href="chemical-spill">Oil Spill Response</a></li>
                                        <li><a href="ship-deck">Ship Deck Equipments</a></li>
                                        <li><a href="respiratory-protection">Industrial Safety Equipments</a></li>
                                        <li><a href="posters-signs">Posters and Safety Signs</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6"> <a href="#"> <img class="img-responsive"
                                                    src="assets/images/img/portfolio-4.jpg" alt=""> </a> </li>
                                        <li class="col-md-6"> <a href="#"> <img class="img-responsive"
                                                    src="assets/images/img/portfolio-41.jpg" alt=""> </a> </li>
                                        <li class="col-md-6"> <a href="#"> <img class="img-responsive"
                                                    src="assets/images/img/portfolio-42.jpg" alt=""> </a> </li>
                                        <li class="col-md-6"> <a href="#"> <img class="img-responsive"
                                                    src="assets/images/img/portfolio-43.jpg" alt=""> </a> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>