<!DOCTYPE html>
<html lang="en">

<head>
    <title>404 Error - ESMAR</title>
    <meta name="description"
        content="The company with 22 years experience in marine safety, Firefighting and lifesaving equipment’s, oil spill response and containment solutions and services in the Middle East We have our branches in Musaffah, Dubai Maritime City and Fujairah.">
    <meta name="keywords"
        content="LSA, FFA, Calibration in Fujairah, Dubai, Sharjah, Abu Dhabi, UAE, Life Saving, Fire Fighting, Oil Spill Response, Ship Deck Equipments, Industrial Safety Equipments, Posters and Safety Signs,
Liferaft Service, Life Saving Appliances Service, Fire Fighting Appliances Service, Lifeboat / Rescue Boat Service, Calibration / Trouble Shooting, Hydraulic Division">
    <meta name="robots" content="ALL" />
    <meta name="author" content="Elite Safety & Marine Supplies L L C, esmarsupplies.com">

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8 bottom-sm-30">
                        <div class="entry-item">
                            <div class="entry-header">

                                <div class="heading-area">
                                    <h3 class="large bottom-0 heading-title"><span>404</span></h3>

                                </div>
                                <div class="entry-content">
                                    <a href="http://esmarsupplies.com/"><img src="assets/images/404.jpg"
                                            class="img-responsive" alt=""></a>
                                </div>
                                <div class="entry-meta text-center top-30">
                                    <div class="row"></div>
                                </div>

                                <div class="entry-relate text-center">
                                    <div class="row"></div>
                                </div>



                            </div>
                        </div>
                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content" style="padding: 0px;margin: 0;">
                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Certifications</strong></span></h4>
                                </div>
                                <div class="widget-content" style="    padding: 10px 15px;">
                                    <ul class="list-unstyled row onepixel sec-container">
                                        <li>
                                            <h5 class="box-item-title">ISO 9001:2015</h5>
                                            <div class="sec-row">
                                                <span class="sec-column-1">
                                                    <img src="./assets/images/img/new_img/abs-iso.png" />
                                                </span>
                                                <span class="sec-column-2">
                                                    <p>An ISO 9001:2015 Certified Company From American Bureau of
                                                        Shipping (ABS)</p>
                                                </span>
                                            </div>
                                        </li>

                                        <li>
                                            <h5 class="box-item-title">OSHAS 18001:2007</h5>
                                            <div class="sec-row">
                                                <span class="sec-column-1">
                                                    <img src="./assets/images/img/new_img/qrs_iso.jpg" />
                                                </span>
                                                <span class="sec-column-2">
                                                    <p>An OSHAS 18001:2007 Certified Company From Quality Register
                                                        Systems (QRS)</p>
                                                </span>
                                            </div>
                                        </li>

                                        <li>
                                            <h5 class="box-item-title">In Country Value</h5>
                                            <div class="sec-row">
                                                <span class="sec-column-1">
                                                    <img src="./assets/images/img/new_img/icv_iso.jpg" />
                                                </span>
                                                <span class="sec-column-2">
                                                    <p>An In Country Value (ICV) Certified Company From ADNOC</p>
                                                </span>
                                            </div>
                                        </li>
                                    </ul>
                                    <img style="width:100%; margin-bottom: 15px;"
                                        src="assets/images/img/new_img/about_image-1.jpg" />

                                </div>
                            </div>
                            <!-- <div class="widget recent-portfolio"> -->
                            <!-- <div class="widget-title bottom-10"> -->
                            <!-- <h4><span>Our <strong>Brands</strong></span></h4> -->
                            <!-- </div>  -->
                            <!-- <div class="widget-content"> -->
                            <!-- <ul class="list-unstyled row onepixel"> -->
                            <!-- <li class="col-md-6"> -->
                            <!-- <a href="#"> -->
                            <!-- <img class="img-responsive" src="assets/images/img/portfolio-4.jpg" alt=""> -->
                            <!-- </a> -->
                            <!-- </li> -->
                            <!-- <li class="col-md-6"> -->
                            <!-- <a href="#"> -->
                            <!-- <img class="img-responsive" src="assets/images/img/portfolio-41.jpg" alt=""> -->
                            <!-- </a> -->
                            <!-- </li> -->
                            <!-- <li class="col-md-6"> -->
                            <!-- <a href="#"> -->
                            <!-- <img class="img-responsive" src="assets/images/img/portfolio-42.jpg" alt=""> -->
                            <!-- </a> -->
                            <!-- </li> -->
                            <!-- <li class="col-md-6"> -->
                            <!-- <a href="#"> -->
                            <!-- <img class="img-responsive" src="assets/images/img/portfolio-43.jpg" alt=""> -->
                            <!-- </a> -->
                            <!-- </li> -->
                            <!-- </ul>  -->
                            <!-- </div>  -->
                            <!-- </div>  -->
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>


</body>

</html>