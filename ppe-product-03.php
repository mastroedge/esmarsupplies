<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div id="product-detail">
                            <div class="row">
                                <div class="col-md-7 bottom-sm-30">
                                    <div class="product-images">
                                        <img src="assets/images/img/product_3_ppe-big.jpg" alt=""
                                            class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h2 class="product-title large thin">Respirators</h2>
                                    <div class="product-review bottom-10">
                                        <span class="star-rating star-rating-4"></span>
                                    </div>

                                    <p class="top-20 bottom-20">Product Description</p>
                                    <div class="product-amount clearfix">

                                        <div class="field">
                                            <button class="button"><a href="enquiry">Enquire Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content top-40">
                                <div class="tabs main border clearfix">
                                    <ul class="tabNavigation list-unstyled bottom-0 clearfix">
                                        <li class="active"><a data-toggle="tab" href="#tab1dc11"><i
                                                    class="fa fa-align-justify icon-left"></i> Product Description</a>
                                        </li>
                                    </ul>
                                    <div id="tab1dc11" class="tabs-container active">
                                        <p>
                                            Brand : 3M</p>
                                        <table>
                                            <tbody>
                                                <tr class="\&quot;oddRow\&quot;">
                                                    <td class="\&quot;label\&quot;">
                                                        Boxes per Case</td>
                                                    <td>
                                                        1</td>
                                                </tr>
                                                <tr>
                                                    <td class="\&quot;label\&quot;">
                                                        Brand</td>
                                                    <td>
                                                        3M Signature Products</td>
                                                </tr>
                                                <tr class="\&quot;oddRow\&quot;">
                                                    <td class="\&quot;label\&quot;">
                                                        Connection Type</td>
                                                    <td>
                                                        Bayonet</td>
                                                </tr>
                                                <tr>
                                                    <td class="\&quot;label\&quot;">
                                                        Facepiece Type</td>
                                                    <td>
                                                        Half Facepiece Reusable</td>
                                                </tr>
                                                <tr class="\&quot;oddRow\&quot;">
                                                    <td class="\&quot;label\&quot;">
                                                        Harness Type</td>
                                                    <td>
                                                        4 point</td>
                                                </tr>
                                                <tr>
                                                    <td class="\&quot;label\&quot;">
                                                        Mission Type</td>
                                                    <td>
                                                        Aircraft, Ships, Vehicles</td>
                                                </tr>
                                                <tr class="\&quot;oddRow\&quot;">
                                                    <td class="\&quot;label\&quot;">
                                                        Operation Type</td>
                                                    <td>
                                                        Facility Safety, Maintenance, Repair &amp; Operations, Overhaul
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="\&quot;label\&quot;">
                                                        Primary Material</td>
                                                    <td>
                                                        Thermoplastic Elastomer (TPE)</td>
                                                </tr>
                                                <tr class="\&quot;oddRow\&quot;">
                                                    <td class="\&quot;label\&quot;">
                                                        Product Type</td>
                                                    <td>
                                                        Half Facepiece</td>
                                                </tr>
                                                <tr>
                                                    <td class="\&quot;label\&quot;">
                                                        Recommended Application</td>
                                                    <td>
                                                        Assembly, Cleaning, Demolition, Facility Maintenance, Grinding,
                                                        Machine Operations, Painting, Sanding, Welding</td>
                                                </tr>
                                                <tr class="\&quot;oddRow\&quot;">
                                                    <td class="\&quot;label\&quot;">
                                                        Recommended Industry</td>
                                                    <td>
                                                        Automotive, Construction, Food &amp; Beverage Manufacturing,
                                                        Manufacturing, Metal Production &amp; Fabrication, Military
                                                        Maintenance, Repair and Operation (MRO), Mining, Oil &amp; Gas,
                                                        Pharmaceutical, Transportation</td>
                                                </tr>
                                                <tr>
                                                    <td class="\&quot;label\&quot;">
                                                        Size</td>
                                                    <td>
                                                        Large</td>
                                                </tr>
                                                <tr class="\&quot;oddRow\&quot;">
                                                    <td class="\&quot;label\&quot;">
                                                        Units per Box</td>
                                                    <td>
                                                        24</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p>&nbsp;
                                        </p>





                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Lifeboat/Life raft (Accessories)</a></li>
                                        <li><a href="respiratory-protection">Respiratory Protection/Public Gas
                                                Detector</a></li>
                                        <li><a href="marine-lights">Marine Lights</a></li>
                                        <li><a href="firefighting">Firefighting Equipment’s</a></li>
                                        <li><a href="respiratory-protection">Safety / PPE Items</a></li>
                                        <li><a href="valves">Valves, Fittings And Flangess</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>