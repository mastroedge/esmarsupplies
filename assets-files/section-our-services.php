<div id="portfolio" class="section medium bg cover top-40 carouselbox">
    <div class="container">
        <div class="main-heading text-center ">
            <h1 class="heading-title wow fadeInDown" data-wow-duration="0.6s" data-wow-delay="0.6s">
                Our <span class="text-orange">Services</span> </h1>
            <h4 class="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">Same project you
                are viewing</h4>
            <hr class="underline-span">
            </hr>
        </div>

        <p class="wow fadeInDown text-center bottom-20" data-wow-duration="0.6s" data-wow-delay="0.8s">We are an
            International Association of Classification Societies
            (IACS) Class approved services provider for Marine LSA & FFA equipments and systems in
            Abu Dhabi, Dubai, Sharjah, Fujairah & Ras Al Khaimah.</p>
        <div class="row">
            <div class="col-md-12">

                <div class="carouselbox nav carousel-control"> <a href="#" class="prev"><i
                            class="fa fa-angle-left"></i></a> <a href="#" class="next"><i
                            class="fa fa-angle-right"></i></a> </div>

                <div class="row">
                    <ul class="carousel-area list-unstyled bottom-0">
                        <li>
                            <div class="portfolio-item">
                                <div class="portfolio-mark">
                                    <div class="portfolio-mark-content">
                                        <div class="portfolio-mark-inner">
                                            <h4 class="bottom-0"><a href="portfolio-detail">Breathing Apparatus /
                                                    EEBD </a></h4>
                                            <a href="#">Service</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-img"> <img src="assets/images/services/thumb/portfolio-1.jpg"
                                        alt="" class="img-responsive"> </div>
                            </div>
                        </li>
                        <li>
                            <div class="portfolio-item">
                                <div class="portfolio-mark">
                                    <div class="portfolio-mark-content">
                                        <div class="portfolio-mark-inner">
                                            <h4 class="bottom-0"><a href="portfolio-detail">Calibration</a></h4>
                                            <a href="#">Service</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-img"> <img src="assets/images/services/thumb/portfolio-3.jpg"
                                        alt="" class="img-responsive"> </div>
                            </div>
                        </li>
                        <li>
                            <div class="portfolio-item">
                                <div class="portfolio-mark">
                                    <div class="portfolio-mark-content">
                                        <div class="portfolio-mark-inner">
                                            <h4 class="bottom-0"><a href="portfolio-detail">
                                                    LifeRaft</a></h4>
                                            <a href="#">Service</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-img"> <img src="assets/images/services/thumb/portfolio-2.jpg"
                                        alt="" class="img-responsive"> </div>
                            </div>
                        </li>
                        <li>
                            <div class="portfolio-item">
                                <div class="portfolio-mark">
                                    <div class="portfolio-mark-content">
                                        <div class="portfolio-mark-inner">
                                            <h4 class="bottom-0"><a href="portfolio-detail">Lifeboat
                                                    Rescue Boat Service</a></h4>
                                            <a href="portfolio-archive">Service</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-img"> <img src="assets/images/services/thumb/portfolio-5.jpg"
                                        alt="" class="img-responsive"> </div>
                            </div>
                        </li>

                        <li>
                            <div class="portfolio-item">
                                <div class="portfolio-mark">
                                    <div class="portfolio-mark-content">
                                        <div class="portfolio-mark-inner">
                                            <h4 class="bottom-0"><a href="portfolio-detail">Fire
                                                    Exitinguisher</a></h4>
                                            <a href="portfolio-archive">Service</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-img"> <img src="assets/images/services/thumb/portfolio-4.jpg"
                                        alt="" class="img-responsive"> </div>
                            </div>
                        </li>


                        <li>
                            <div class="portfolio-item">
                                <div class="portfolio-mark">
                                    <div class="portfolio-mark-content">
                                        <div class="portfolio-mark-inner">
                                            <h4 class="bottom-0"><a href="portfolio-detail">Fixed
                                                    Co2 System Inspection & Certification</a></h4>
                                            <a href="portfolio-archive">Service</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-img"> <img src="assets/images/services/thumb/portfolio-6.jpg"
                                        alt="" class="img-responsive"> </div>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>