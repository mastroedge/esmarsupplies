<div id="blog" class="section medium top-40 carouselbox">
                        <div class="container">
                            <div class="main-heading text-center ">
                                <h1 class="heading-title wow fadeInDown" data-wow-duration="0.6s" data-wow-delay="0.6s">
                                    Our <span class="text-orange">Approvals</span> </h1>
                                <h4 class="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">Sometime we want
                                    to share something</h4>
                                <hr class="underline-span">
                                </hr>
                            </div>
                            <p class="wow fadeInDown text-center bottom-20" data-wow-duration="0.6s"
                                data-wow-delay="0.8s">We are Approved LSA & FFA Service Providers by the below IACS
                                Classes, ADNOC & FTA as well.</p>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="carouselbox nav carousel-control">
                                        <a href="#" class="prev"><i class="fa fa-angle-left"></i></a>
                                        <a href="#" class="next"><i class="fa fa-angle-right"></i></a>
                                    </div>
                                    <div class="row">
                                        <ul class="carousel-area list-unstyled bottom-0">
                                            <li>
                                                <div class="blog-item">
                                                    <div class="blog-image">
                                                        <div class="blog-mark">
                                                            <div class="blog-mark-content">
                                                                <div class="blog-mark-content-inc"><i
                                                                        class="fa fa-search"></i></div>
                                                            </div>
                                                        </div>
                                                        <img src="assets/images/approvals/approvals-09.jpg" alt=""
                                                            class="img-responsive">
                                                    </div>
                                                    <div class="blog-content">

                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="blog-item">
                                                    <div class="blog-image">
                                                        <div class="blog-mark">
                                                            <div class="blog-mark-content">
                                                                <div class="blog-mark-content-inc"> <i
                                                                        class="fa fa-search"></i> </div>
                                                            </div>
                                                        </div>
                                                        <img src="assets/images/approvals/approvals-01.jpg" alt=""
                                                            class="img-responsive">
                                                    </div>
                                                    <div class="blog-content">

                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="blog-item">
                                                    <div class="blog-image">
                                                        <div class="blog-mark">
                                                            <div class="blog-mark-content">
                                                                <div class="blog-mark-content-inc"> <i
                                                                        class="fa fa-search"></i> </div>
                                                            </div>
                                                        </div>
                                                        <img src="assets/images/approvals/approvals-02.jpg" alt=""
                                                            class="img-responsive">
                                                    </div>
                                                    <div class="blog-content">

                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="blog-item">
                                                    <div class="blog-image">
                                                        <div class="blog-mark">
                                                            <div class="blog-mark-content">
                                                                <div class="blog-mark-content-inc"><i
                                                                        class="fa fa-search"></i> </div>
                                                            </div>
                                                        </div>
                                                        <img src="assets/images/approvals/approvals-03.jpg" alt=""
                                                            class="img-responsive">
                                                    </div>
                                                    <div class="blog-content">

                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="blog-item">
                                                    <div class="blog-image">
                                                        <div class="blog-mark">
                                                            <div class="blog-mark-content">
                                                                <div class="blog-mark-content-inc"> <i
                                                                        class="fa fa-search"></i> </div>
                                                            </div>
                                                        </div>
                                                        <img src="assets/images/approvals/approvals-04.jpg" alt=""
                                                            class="img-responsive">
                                                    </div>
                                                    <div class="blog-content">

                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="blog-item">
                                                    <div class="blog-image">
                                                        <div class="blog-mark">
                                                            <div class="blog-mark-content">
                                                                <div class="blog-mark-content-inc"><i
                                                                        class="fa fa-search"></i></div>
                                                            </div>
                                                        </div>
                                                        <img src="assets/images/approvals/approvals-05.jpg" alt=""
                                                            class="img-responsive">
                                                    </div>
                                                    <div class="blog-content">

                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="blog-item">
                                                    <div class="blog-image">
                                                        <div class="blog-mark">
                                                            <div class="blog-mark-content">
                                                                <div class="blog-mark-content-inc"><i
                                                                        class="fa fa-search"></i></div>
                                                            </div>
                                                        </div>
                                                        <img src="assets/images/approvals/approvals-06.jpg" alt=""
                                                            class="img-responsive">
                                                    </div>
                                                    <div class="blog-content">

                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="blog-item">
                                                    <div class="blog-image">
                                                        <div class="blog-mark">
                                                            <div class="blog-mark-content">
                                                                <div class="blog-mark-content-inc"><i
                                                                        class="fa fa-search"></i></div>
                                                            </div>
                                                        </div>
                                                        <img src="assets/images/approvals/approvals-07.jpg" alt=""
                                                            class="img-responsive">
                                                    </div>
                                                    <div class="blog-content">

                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="blog-item">
                                                    <div class="blog-image">
                                                        <div class="blog-mark">
                                                            <div class="blog-mark-content">
                                                                <div class="blog-mark-content-inc"><i
                                                                        class="fa fa-search"></i></div>
                                                            </div>
                                                        </div>
                                                        <img src="assets/images/approvals/approvals-08.jpg" alt=""
                                                            class="img-responsive">
                                                    </div>
                                                    <div class="blog-content">

                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="blog-item">
                                                    <div class="blog-image">
                                                        <div class="blog-mark">
                                                            <div class="blog-mark-content">
                                                                <div class="blog-mark-content-inc"><i
                                                                        class="fa fa-search"></i></div>
                                                            </div>
                                                        </div>
                                                        <img src="assets/images/approvals/approvals-10.jpg" alt=""
                                                            class="img-responsive">
                                                    </div>
                                                    <div class="blog-content">

                                                    </div>
                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>