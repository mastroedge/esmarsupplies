<div class="container m-t-25">
    <div class="col-md-12 feature-box-area">

        <div class="main-heading text-center ">
            <h1 class="heading-title wow fadeInDown" data-wow-duration="0.6s" data-wow-delay="0.6s">
                About <span class="text-orange">our brands</span> </h1>
            <h4 class="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">“We are
                dedicated to achieving excellence in our work”</h4>
            <hr class="underline-span">
            </hr>
        </div>

        <div class="box-container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box-item wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.3s">
                        <div class="box-item-heading">
                            <h5 class="box-item-title">DAWG (USA) – Oil Spill Response Kits and
                                Absorbents </h5>
                        </div>
                        <div class="box-item-body">What Products We Sell...<br>
                            • Dawg® offers a large selection of products to help keep your workplace
                            safe and clean Absorbents - like absorbent mats, socks, pillows, wipers
                            and booms to soak up leaks, drips and spills.
                            <div id="read-more1" class="collapse">
                                • Spill Kits - to help you respond quickly to different types and
                                sizes of spills. </br>
                                • Spill Containment Products - like spill pallets and decks, tanks
                                and berms </br>
                                • Material Handling Products - like safety cans and cabinets to
                                store and contain flammable liquids, as well as drums, overpacks and
                                drum moving equipment.
                            </div>
                            <a class="read-more" data-toggle="collapse" data-target="#read-more1">more...</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-item wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                        <div class="box-item-heading">
                            <h5 class="box-item-title">ALBATROSS (ITALY) - Pyrotechnics</h5>
                        </div>
                        <div class="box-item-body">The ALBATROSS was established in 1997 by
                            Gallinoro family. The Gallinoro have always been very keen on sea and
                            they have transferred their passion to the nautical sector.
                            <div id="read-more5" class="collapse">
                                Nowadays ALBATROSS is one of the market leaders in Europe in the
                                Distress Signals used on Ships, Lifeboats & Liferafts.
                                The range of products has gradually increased and now ALBATROSS
                                produces all kinds of signs demanded by the market in this sector.
                            </div>
                            <a class="read-more" data-toggle="collapse" data-target="#read-more5">more...</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box-item wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                        <div class="box-item-heading">
                            <h5 class="box-item-title">VELERIA SAN GIORGIO VSG (ITALY) – Lifejackets
                                & Lifesaving Equipment’s</h5>
                        </div>
                        <div class="box-item-body">
                            VSG is the oldest and well-known Italian company in the design and
                            production of lifejackets.
                            <div id="read-more6" class="collapse">
                                This company confirms once again to be always at the forefront
                                showing the new product range certified by RINA (Italian Naval
                                Registry) in accordance with EN ISO 12402. </p>
                                <p> VSG had a lead role on the Italian nautical field from 1926
                                    until today guaranteeing innovation and quality during all
                                    different steps of constant development of the market.
                                    VSG begun manufacturing sails and waxed clothes for merchant
                                    ships in transit at Genoa port and then she begun to supply the
                                    Navy and considering the gradual mechanization of ships, she
                                    diversified the production realizing flags and lifejackets: this
                                    choice has been strategic in the 60's thanks to the pleasure
                                    craft development.</p>
                            </div>
                            <a class="read-more" data-toggle="collapse" data-target="#read-more6">more...</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-item wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                        <div class="box-item-heading">
                            <h5 class="box-item-title">ESCHBACH (GERMANY) – Firefighting Hoses</h5>
                        </div>
                        <div class="box-item-body">
                            Established in 1956 the family owned Jakob Eschbach GmbH became one of
                            the largest producers of layflat hoses worldwide.
                            <div id="read-more7" class="collapse">
                                <p> Top quality materials, newest machinery, permanent research and
                                    a well trained and experienced labour force are our base leaving
                                    to satisfy customers all over the world. We offer a wide range
                                    of layflat hoses, which are completely made in Germany, meeting
                                    all requirements of firefighting operations as well as special
                                    constructions for irrigation, mining, refineries, ships etc.</p>
                            </div>
                            <a class="read-more" data-toggle="collapse" data-target="#read-more7">more...</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="box-item wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                        <div class="box-item-heading">
                            <h5 class="box-item-title">ABS Srl / ANAF (ITALY) – Marine Fire
                                Extinguishers & Marine Fixed Fire Suppression Systems</h5>
                        </div>
                        <div class="box-item-body">
                            a.b.s. firefighting has been founded in 1999 by highly qualified staff
                            with thirty year’s experience in national and international markets.
                            <div id="read-more8" class="collapse">
                                Today it operates in the field of fire prevention, obtaining
                                considerable success, offering to its clients high and qualifying
                                firefighting systems solutions; thanks to the prepared technical
                                staff is always taking care to technological innovation. Its
                                operational and organizational structure enables a.b.s. firefighting
                                to offer to its customers the specific solutions to any kind of
                                problem always having a special attention to the environment.</p>
                            </div>
                            <a class="read-more" data-toggle="collapse" data-target="#read-more8">more...</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box-item wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                        <div class="box-item-heading">
                            <h5 class="box-item-title">AUSMAR (SPAIN) – DURRAY Liferafts</h5>
                        </div>
                        <div class="box-item-body">
                            Manufacturer of the prestigious brand Duarry and with over 40 year’s
                            experience in the maritime safety field, Ausmar has the most extensive
                            network of liferafts service stations in Spain.
                            <div id="read-more9" class="collapse">
                                <p>Ausmar is a renowned company in the maritime safety field thanks
                                    not only to the value of its large network of servicing stations
                                    but also to the knowledge their technicians and employees have.
                                </p>
                                <p>With a large career and a great experience in the sector, Ausmar
                                    is still committed to its clients, who expect from the company
                                    the best products and a better service.</p>
                            </div>
                            <a class="read-more" data-toggle="collapse" data-target="#read-more9">more...</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box-item wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                        <div class="box-item-heading">
                            <h5 class="box-item-title">KHERAJ (INDIA) - Industrial Electrical Sirens
                            </h5>
                        </div>
                        <div class="box-item-body">
                            <span>“Kheraj”, owned by Kheraj Electrical Industries (P) Ltd, is a
                                brand that has been synonymous with sirens for over six decades. We
                                have made our mark in several factories, banks, ships, oil
                                refineries,</span>
                            <span id="read-more10" class="collapse">
                                workshops, prayer halls, and corporate plants globally. Further, our
                                alarm systems have been fitted in fire engines, police vehicles, and
                                ambulances to name a few.
                            </span>
                            <a class="read-more" data-toggle="collapse" data-target="#read-more10">more...</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-item wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                        <div class="box-item-heading">
                            <h5 class="box-item-title">PROTEK (TAIWAN) – Firefighting Nozzles,
                                Monitors etc.</h5>
                        </div>
                        <div class="box-item-body">
                            Protek Manufacturing Corp. is a leader in providing high performance
                            firefighting equipment.
                            Since 1972, Protek has adhered to three operating principles of
                            delivering high-quality products,
                            <div id="read-more11" class="collapse">
                                <p>creating value for our customers and providing exceptional
                                    service. Protek is constantly improving existing products and
                                    developing new products in order to meet the challenges of the
                                    fire industry. From high pressure self-educting nozzles to
                                    wireless remote-control monitors, we have continually developed
                                    products that are tailored to the evolving needs of our
                                    customers.</p>
                            </div>
                            <a class="read-more" data-toggle="collapse" data-target="#read-more11">more...</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="box-item wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                        <div class="box-item-heading">
                            <h5 class="box-item-title">FASTCALGAS (UK) – Calibration Gas Bottles
                            </h5>
                        </div>
                        <div class="box-item-body">
                            Originally introduced to market in 2010, FastCalGas service offers a
                            completely stress-free experience. A one-stop-shop when it comes to
                            procuring calibration/span and quad gas, we ensure that gas is delivered
                            straight
                            <div id="read-more12" class="collapse">to your vessel when you want it.
                                Our fully managed service means that we know when your gas is about
                                to run out and we will alert you of this in appropriate time…say
                                good bye to any nasty surprises! Customers can purchase quickly and
                                easily online, anytime and anywhere in world. Breaking from the
                                norm, we’re the only calibration gas supplier offering this unique
                                service.
                            </div>
                            <a class="read-more" data-toggle="collapse" data-target="#read-more12">more...</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-item wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                        <div class="box-item-heading">
                            <h5 class="box-item-title">VARUNEY (INDIA) – Firefighting Fittings</h5>
                        </div>
                        <div class="box-item-body">
                            Ours is leading concern specialized in products manufacturer / Exporter
                            and supplier of Fire Protection, Fire Fighting, Resisting Equipment’s,
                            by moving of water in a controlled manner.
                            <div id="read-more13" class="collapse">
                                The said products are manufactured as per relevant Indians / British
                                / American / DIN Standards. With basic material Gun Metal / Aluminum
                                / Stainless Steel, alternatively used as per demand of client and
                                usage.
                                <p>The company started since 1989, and is from one of the
                                    association group of Indian pioneer company, M/s. Shah Bhogilal
                                    Jethalal & Brothers since 1933, which is working since seven
                                    decades. Ours basic product range is being shown in our next
                                    page of product. They are mainly Landing Valves, Couplings,
                                    Nozzles, Adaptors and all Fire Fighter (Tender) Fittings and
                                    project / High rise building wet and dry risers installation
                                    material.</p>
                            </div>
                            <a class="read-more" data-toggle="collapse" data-target="#read-more13">more...</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Brand Logos -->
            <div class="section medium  carouselbox about-sec">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="carouselbox nav carousel-control"> <a href="#" class="prev"><i
                                        class="fa fa-angle-left"></i></a> <a href="#" class="next"><i
                                        class="fa fa-angle-right"></i></a> </div>
                            <div class="row">
                                <ul class="carousel-area list-unstyled bottom-0">
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-01.jpg"
                                                    alt="Albatross Distress Signals" class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-02.jpg" alt="Arimar"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-03.jpg" alt="ESCHBACH"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-04.jpg" alt="Mandals"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-05.jpg" alt="Carlos Arboles"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-06.jpg" alt="Centurion"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-07.jpg" alt="KHERAJ"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-08.jpg" alt="Safe T Made"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-09.jpg" alt="Trellchem"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-10.jpg" alt="ABS"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-11.jpg"
                                                    alt="Anaf Fire Protection" class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-12.jpg" alt="ULTRATECH"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-13.jpg" alt="Alcofind"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-14.jpg" alt="AAAG"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>


                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-15.jpg" alt="Protek"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>


                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-16.jpg" alt="JacTone"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>


                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-17.jpg" alt="Varuney"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-18.jpg" alt="FastCalGas"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-19.jpg" alt="Jonesco"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-20.jpg" alt="Mausmar"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-21.jpg" alt="CSM"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-22.jpg"
                                                    alt="Shanghai Youlong Rubber" class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-23.jpg" alt="SEA-AIR"
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-24.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-25.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-26.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-27.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-28.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-29.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>


                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-30.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-31.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-32.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-33.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-34.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-35.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-36.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-37.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-38.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-39.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-40.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-41.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-42.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-43.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-44.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-45.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-46.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-47.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-48.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="blog-item">
                                            <div class="blog-image">
                                                <div class="blog-mark">
                                                    <div class="blog-mark-content">
                                                        <div class="blog-mark-content-inc"><i class="fa fa-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="assets/images/brands/logo-new-49.jpg" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="blog-content"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>