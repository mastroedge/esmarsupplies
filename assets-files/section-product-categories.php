<section>
    <div class="container">
        <div class="col-md-12 bottom-sm-30">
            <div class="main-heading text-center">
                <h1 class="heading-title wow fadeInDown" data-wow-duration="0.6s" data-wow-delay="0.6s">OUR <span
                        class="text-orange">PRODUCT</span> CATEGORIES</h1>
                <h4 class="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">Experience Real Time Event
                    Tracking</h4>
                <hr class="underline-span">
                </hr>
            </div>
            <p class="wow fadeInDown text-center" data-wow-duration="0.6s" data-wow-delay="0.8s">Elite Safety & Marine
                Supplies LLC (ESMAR) takes this opportunity to introduce ourselves as a leading company for all kinds of
                Marine, Safety in Oil and Gas, Firefighting, Safety Signage and Personal Protective Equipment (under the
                parent company group ‘Essa Marine Equipment LLC’ established since 1996).</p>

            <div class="row top-30 product-list">

                <div class="col-md-4 bottom-20">
                    <a href="lifeboat-liferaft" class="card bg-light wow fadeInUp text-center" data-wow-duration="0.4s"
                        data-wow-delay="1s">
                        <div class="icon"> <img src="assets/images/img/new_img/product_01.png"> </div>
                        <div class="icon-content ">
                            <h3>Life Saving</h3>
                            <span>Life Saving Equipments</span>
                        </div>
                    </a>
                </div>

                <div class="col-md-4 bottom-20">
                    <a href="firefighting" class="card bg-light wow fadeInUp text-center" data-wow-duration="0.4s"
                        data-wow-delay="1.2s">
                        <div class="icon"> <img src="assets/images/img/new_img/product_02.jpg"> </div>
                        <div class="icon-content">
                            <h3>Fire Fighting</h3>
                            <span>Fire Fighting Equipments</span>
                        </div>
                    </a>
                </div>

                <div class="col-md-4 bottom-20">
                    <a href="chemical-spill" class="card bg-light wow fadeInUp text-center" data-wow-duration="0.4s"
                        data-wow-delay="1.4s">
                        <div class="icon"> <img src="assets/images/img/new_img/product_03.jpg"> </div>
                        <div class="icon-content">
                            <h3>Oil Spill Response</h3>
                            <span>Oil Spill Response Equipments</span>
                        </div>
                    </a>
                </div>

                <div class="col-md-4 bottom-20">
                    <a href="ship-deck" class="card bg-light wow fadeInUp text-center" data-wow-duration="0.4s"
                        data-wow-delay="1.6s">
                        <div class="icon"> <img src="assets/images/img/new_img/product_04.jpg"> </div>
                        <div class="icon-content">
                            <h3>Ship Deck Equipments</h3>
                            <span>Ship Deck Equipments</span>
                        </div>
                    </a>
                </div>

                <div class="col-md-4 bottom-20">
                    <a href="respiratory-protection" class="card bg-light wow fadeInUp text-center"
                        data-wow-duration="0.4s" data-wow-delay="1.8s">
                        <div class="icon"> <img src="assets/images/img/new_img/product_05.png"></div>
                        <div class="icon-content">
                            <h3>Industrial Safety Equipments</h3>
                            <span>Latest Industrial Safety Equipments</span>
                        </div>
                    </a>
                </div>

                <div class="col-md-4 bottom-20">
                    <a href="posters-signs" class="card bg-light wow fadeInUp text-center" data-wow-duration="0.6s"
                        data-wow-delay="1.3s">
                        <div class="icon"><img src="assets/images/img/posters.jpg"> </div>
                        <div class="icon-content">
                            <h3>Posters and Safety Signs</h3>
                            <span>Posters and Safety Signs Items</span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="text-center wow fadeInUp bottom-20" data-wow-duration="0.6s" data-wow-delay="1s"><a
                    href="lifeboat-liferaft" class="btn btn-custom bg-orange">View all</a></div>
        </div>
    </div>
</section>