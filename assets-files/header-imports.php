<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/fonts.css" rel="stylesheet">
<link href="assets/css/animate.css" rel="stylesheet">
<link href="assets/css/plugin.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">
<link id="erange-color" href="assets/css/color/default.css" rel="stylesheet">
<link href="assets/css/demo.css" rel="stylesheet">
<link href="assets/rs-plugin/css/settings.css" rel="stylesheet">
<link href="assets/rs-plugin/css/captions.css" rel="stylesheet">

<script src="assets/js/jquery.js"></script>
<!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <script src="assets/js/respond.min.js"></script>
<![endif]-->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-169255604-1"></script>
<script>
if (window.location.hostname != "localhost") {
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'UA-169255604-1');
}
</script>