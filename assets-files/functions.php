<?php
$url_array =  explode('/', $_SERVER['REQUEST_URI']) ;

// Constants
define('CURRENT_URL', end($url_array));
define('BASE_URL', 'http://esmarsupplies.com');

function active($current_url, $currect_page, $sub_menu = array()) {
    if($current_url == BASE_URL || $current_url == $currect_page){
        return 'active'; //class name in css 
    } elseif(count($sub_menu) > 0) {
        return in_array_r($current_url, $sub_menu) ? 'active' : '';
    }
}
function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
    return false;
}
?>