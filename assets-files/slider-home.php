<div class="w3-content w3-display-container video-sec">
    <!--   <a class="skip-main" href="#content"><i class="fa fa-angle-down"></i></a> -->
    <div class="main-heading text-left video-text">
        <h1 class="heading-title wow fadeInDown" data-wow-duration="2.0s" data-wow-delay="0.60s">YOUR <span
                class="text-orange">SAFETY AT SEA</span></br> IS OUR FIRST PRIORITY</h1>
    </div>
    <video autoplay muted loop id="myVideo">
        <source src="assets/video/cru.mp4" type="video/mp4">
        Your browser does not support HTML5 video.
    </video>
</div>