<div id="callout" class="callout-new">
    <div class="container">
        <div class="row">
            <div class="col-md-8 bottom-sm-20">
                <div class="heading-area wow flipInX" data-wow-duration="0.6s" data-wow-delay="0.3s">
                    <h5 class="large bottom-0 heading-title white">Expansive product line sure to meet
                        your needs</h5>
                    <h4 class="heading-subtitle bottom-0 white">Contact Us today to learn more</h4>
                </div>
            </div>
            <div class="col-md-4 text-right text-sm-left"> <a href="contact"
                    class="btn btn-white btn-custom wow flipInY" data-wow-duration="0.6s" data-wow-delay="0.3s">Click
                    Here</a> </div>
        </div>
    </div>
</div>