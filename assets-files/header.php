<?php
include_once('./assets-files/functions.php');
// Read JSON file
$menu_json = file_get_contents('./assets-files/menu.json');

//Decode JSON
$menu_data = json_decode($menu_json,true);

//Print data
// echo '<pre>';print_r($menu_data);echo '</pre>';
?>
<header id="header" class="no-bg">
    <div class="topheader">
        <div class="container">
            <div class="row"> </div>
        </div>
    </div>
    <div class="mainheader fixedmenu" role="banner">
        <div class="container">
            <div class="header-inner clearfix">
                <div class="logo pull-left text-sm-center">
                    <h1 class="site-title"> <a href="http://esmarsupplies.com"><img
                                src="assets/images/img/esmar-logo.png"></a> </h1>
                </div>
                <div class="site-menu border-style pull-right hidden-sm hidden-xs">
                    <nav role="navigation">
                        <ul class="sf-menu nav nav-tabs clearfix">
                        <?php
                        foreach($menu_data as $menu) {
                            $sub_menu = isset($menu['sub']) ? $menu['sub'] : array();
                            echo '<li class="menu-normal '.active(CURRENT_URL, $menu['href'], $sub_menu).'">';
                            echo '<a href="'.$menu['href'].'">'.$menu['name'].'</a>';
                            if(count($sub_menu) > 0) {
                                echo '<ul>';
                                foreach($menu['sub'] as $sub) {
                                    echo '<li class="'.active(CURRENT_URL, $sub['href']).'"><a href="'.$sub['href'].'">'.$sub['name'].'</a></li>';
                                }
                                echo '</ul>';
                            }
                            echo '</li>';
                        }
                        ?>
                    </nav>
                    <span class="header-search"></span>
                </div>
            </div>
        </div>
    </div>
</header>