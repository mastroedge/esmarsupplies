<footer id="footer" role="contentinfo">
    <div class="widgetarea">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="widget bottom-sm-30">
                        <div class="widget-title">
                            <h4>About ESMAR</h4>
                        </div>
                        <p>Elite Safety &Marine Supplies LLC (ESMAR) covers the safety and firefighting segment of
                            marine and oil & gas industry. ESMAR was found in Abu Dhabi in 2012 under the umbrella of
                            Essa Marine; which was established in 1997</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget taxonomy-list bottom-sm-30">
                        <div class="widget-title">
                            <h4>Our Products</h4>
                        </div>
                        <ul class="bottom-0 list-unstyled">
                            <li><a href="lifeboat-liferaft">Life Saving</a></li>
                            <li><a href="firefighting">Fire Fighting</a></li>
                            <li><a href="chemical-spill">Oil Spill Response</a></li>
                            <li><a href="ship-deck">Ship Deck Equipments</a></li>
                            <li><a href="respiratory-protection">Industrial Safety Equipments</a></li>
                            <li><a href="posters-signs">Posters and Safety Signs</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget taxonomy-list bottom-sm-30">
                        <div class="widget-title">
                            <h4>Our Services</h4>
                        </div>
                        <ul class="bottom-0 list-unstyled">
                            <li><a href="liferaft-service">Liferaft Service</a></li>
                            <li><a href="life-saving-appliances-service">Life Saving Appliances Service</a></li>
                            <li><a href="fire-fighting-service">Fire Fighting Appliances Service</a></li>
                            <li><a href="lifeboat-rescue-service">Lifeboat / Rescue Boat Service</a></li>
                            <li><a href="trouble-shooting-service">Calibration / Trouble Shooting</a></li>
                            <li><a href="hydraulic-fittings">Hydraulic Division</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget tags-cloud clearfix bottom-sm-30">
                        <div class="widget-title">
                            <h4>Contact</h4>
                        </div>

                        <ul class="bottom-0 list-unstyled">
                            <li>Elite Safety &amp; Marine Supplies L L C</a></li>
                            <li>Store#58, Plot#8, Musaffah Industrial Area,</li>
                            <li>M20, Abu Dhabi, UAE</li>
                            <li>Tel: +971 2 554 4315</li>
                            <li>Fax: +971 2 554 3122</li>
                            <li>P. O. box 92839</li>
                            <li>Email: sales@esmarsupplies.com</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="credit">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 text-xs-center bottom-xs-5">
                    <p>&copy; Elite Safety & Marine Supplies LLC 2020 All rights reserved</p>
                </div>
                <div class="col-md-6 col-sm-6 creditlink text-xs-center">
                    <div class="topsocial">
                        <ul class="list-unstyled bottom-0 social border">
                            <li class="facebook"><a href="https://www.facebook.com/elitesafetymarine/" target="_blank"
                                    data-placement="top" data-toggle="tooltip" title="Facebook"><i
                                        class="fa fa-facebook"></i></a></li>
                            <li class="twitter"><a href="#" data-placement="top" data-toggle="tooltip"
                                    title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li class="googleplus"><a href="#" data-placement="top" data-toggle="tooltip"
                                    title="Instagram"><i class="fa fa-instagram"></i></a></li>
                            <li class="pinterest"><a href="#" data-placement="top" data-toggle="tooltip"
                                    title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                            <li class="linkedin"><a href="https://www.linkedin.com/company/esmarsupplies/"
                                    target="_blank" data-placement="top" data-toggle="tooltip" title="LinkedIn"><i
                                        class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <ul class="bottom-0 list-unstyled clearfix">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<a href="#mobile-menu" class="responsive-menu visible-xs visible-sm button pull-right color"><i
        class="fa fa-bars"></i></a>
<div id="mobile-menu"> <span class="menu-content"></span> </div>