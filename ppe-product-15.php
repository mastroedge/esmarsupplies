<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div id="product-detail">
                            <div class="row">
                                <div class="col-md-7 bottom-sm-30">
                                    <div class="product-images">
                                        <img src="assets/images/img/product_5_ppe-big.jpg" alt=""
                                            class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h2 class="product-title large thin">Eye Wash Showers</h2>
                                    <div class="product-review bottom-10">
                                        <span class="star-rating star-rating-4"></span>
                                    </div>

                                    <p class="top-20 bottom-20">Product Description</p>
                                    <div class="product-amount clearfix">

                                        <div class="field">
                                            <button class="button"><a href="enquiry">Enquire Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content top-40">
                                <div class="tabs main border clearfix">
                                    <ul class="tabNavigation list-unstyled bottom-0 clearfix">
                                        <li class="active"><a data-toggle="tab" href="#tab1dc11"><i
                                                    class="fa fa-align-justify icon-left"></i> Product Description</a>
                                        </li>
                                    </ul>
                                    <div id="tab1dc11" class="tabs-container active">

                                        <p>&nbsp;
                                        </p>
                                        <p>&nbsp;
                                        </p>
                                        <p>
                                            Brand : Carlos Arboles (Spain)centurion (Taiwan)</p>
                                        <p>&nbsp;
                                        </p>
                                        <p>
                                            HIGH QUALITY MATERIALS</p>
                                        <p>
                                            Materials in contact with water are galvanized</p>
                                        <p>
                                            steel pipes, brass fittings and ABS reinforced plastic.</p>
                                        <p>
                                            We also have the ability to manufacture equipment</p>
                                        <p>
                                            in stainless steel .</p>
                                        <p>
                                            1</p>
                                        <p>
                                            2</p>
                                        <p>
                                            ROBUST CONSTRUCTION</p>
                                        <p>
                                            All our products are constructed from high quality</p>
                                        <p>
                                            materials, where fastening is surface mounting the</p>
                                        <p>
                                            components are always metallic.</p>
                                        <p>
                                            This results into a robust equipment for the most</p>
                                        <p>
                                            adverse conditions.</p>
                                        <p>
                                            SHOWER HEAD</p>
                                        <p>
                                            &bull; The water distribution pattern is in accordance</p>
                                        <p>
                                            to all relevant standards to give a wide and</p>
                                        <p>
                                            smooth flow pattern.</p>
                                        <p>
                                            &bull; The large Shower head diameter of 250 mm and</p>
                                        <p>
                                            the design of the flow holes prevent restricted</p>
                                        <p>
                                            flow caused by lime scale or contamination.</p>
                                        <p>
                                            3</p>
                                        <p>
                                            5</p>
                                        <p>
                                            EYEWASH ASSEMBLY</p>
                                        <p>
                                            One or two spray heads offer a low pressure aerated water, which flushes the
                                            face and the eyes without</p>
                                        <p>
                                            damaging the delicate eye tissues.</p>
                                        <p>
                                            The large-diameter jet reaches a 15 cm height, according to the standard.
                                            The plastic materials and rounded</p>
                                        <p>
                                            forms will not cause facial damage in the case of accidental contact. They
                                            include an anti-dust cover which</p>
                                        <p>
                                            operates automatically when the Eyewash unit is activated.</p>
                                        <p>
                                            ANTI-CORROSIVE AND ENVIRONMENTAL COATING</p>
                                        <p>
                                            Anti-corrosive polyamide 11 plastic coating, in HIGH-VISIBILITY YELLOW
                                            color. This plastic coating has</p>
                                        <p>
                                            a thickness of 250 to 300 microns. It has good resistance to acids, bases,
                                            sea water, oils...</p>
                                        <p>
                                            The coating is not derived from petroleum, but is a raw material originating
                                            from vegetable oil, and therefore,</p>
                                        <p>
                                            it preserves the environment.</p>
                                        <p>
                                            We have a Chemical Resistance Table, which can be consulted on our webpage
                                        </p>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Life Saving</a></li>
                                        <li><a href="firefighting">Fire Fighting</a></li>
                                        <li><a href="chemical-spill">Oil Spill Response</a></li>
                                        <li><a href="ship-deck">Ship Deck Equipments</a></li>
                                        <li><a href="respiratory-protection">Industrial Safety Equipments</a></li>
                                        <li><a href="posters-signs">Posters and Safety Signs</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>