<!DOCTYPE html>
<html lang="en">

<head>
    <title>LSA, FFA, Calibration in Fujairah, Dubai, Sharjah, Abu Dhabi, UAE</title>
    <meta name="description"
        content="The company with 22 years experience in marine safety, Firefighting and lifesaving equipment’s, oil spill response and containment solutions and services in the Middle East We have our branches in Musaffah, Dubai Maritime City and Fujairah.">
    <meta name="keywords"
        content="LSA, FFA, Calibration in Fujairah, Dubai, Sharjah, Abu Dhabi, UAE, Life Saving, Fire Fighting, Oil Spill Response, Ship Deck Equipments, Industrial Safety Equipments, Posters and Safety Signs,
Liferaft Service, Life Saving Appliances Service, Fire Fighting Appliances Service, Lifeboat / Rescue Boat Service, Calibration / Trouble Shooting, Hydraulic Division">
    <meta name="robots" content="ALL" />
    <meta name="author" content="Elite Safety & Marine Supplies L L C, esmarsupplies.com">

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8 bottom-sm-30">
                        <div class="entry-item">
                            <div class="entry-header">

                                <div class="heading-area">
                                    <h3 class="large bottom-0 heading-title"><span>ABOUT US</span></h3>

                                </div>
                                <div class="entry-media bottom-20">
                                    <a href="#"><img src="assets/images/img/sliderbox-3.jpg" class="img-responsive"
                                            alt=""></a>
                                </div>
                                <div class="entry-content">
                                    <p>Elite Safety & Marine Supplies LLC (ESMAR) specializes in FFA & LSA inspection
                                        and new supply, as
                                        required by SOLAS, flag regulations and Class requirement, approved by class:
                                        ABS, BV, DNV, LR, GL,
                                        IRS, TASNEEF. Make sure each ship maintenance inspection done by us comply with
                                        relevant
                                        requirements with advanced facilities, professional maintenance team,
                                        diversified channel and
                                        excellent quality management system ISO 9001:2015. We are Approved by Abu Dhabi
                                        National Oil Company
                                        (ADNOC) and Federal Transport Authority (FTA) also for the LSA & FFA Inspection
                                        & Certification.</p>
                                    <p>ESMAR was found in Abu Dhabi in 2012 under the umbrella of Essa Marine; which was
                                        established in
                                        1997 as retail and a wholesale entity – compiled together is a family; a
                                        workforce of customer
                                        oriented and content driven staff that have an experience in this nature of
                                        industry for over 30
                                        years. The experience of our staff adds to their knowledge of Quality products
                                        and the originality
                                        and legitimacy of products. We offer our client the convenience of complete
                                        solution to their health
                                        & safety under one roof.</p>

                                    <p>“We are dedicated to achieving excellence in our work”</p>
                                    <p>“The company with 22 years” experience in marine safety, Firefighting and
                                        lifesaving equipment’s,
                                        oil spill response and containment solutions and services in the Middle East”.
                                        We have our branches
                                        in Musaffah, Dubai Maritime City and Fujairah.</p>
                                    <p>2014 In Dubai Maritime City -Dubai we opened our first Service Centre, Emprise
                                        Safety & Marine
                                        Supplies L.L.C. (2014) to provide service of Fire Fighting & Life Saving
                                        Equipment’s for both
                                        Onshore & Offshore.</p>
                                    <p>2019 In Fujairah – Inside Fujairah Port we opened our New Branch to, Elite Saftey
                                        & Marine Supplies
                                        L.L.C (2019) - Fujairah Branch cater and service our clients requirements in
                                        entire UAE regions &
                                        International clientele. Here we are able to attend the needs / requirements of
                                        Vessels calling at
                                        Fujairah Anchorage.</p>
                                </div>
                                <div class="entry-meta text-center top-30">
                                    <div class="row"></div>
                                </div>

                                <div class="entry-relate text-center">
                                    <div class="row"></div>
                                </div>



                            </div>
                        </div>
                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content" style="padding: 0px;margin: 0;">
                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Certifications</strong></span></h4>
                                </div>
                                <div class="widget-content" style="    padding: 10px 15px;">
                                    <ul class="list-unstyled row onepixel sec-container">
                                        <li>
                                            <h5 class="box-item-title">ISO 9001:2015</h5>
                                            <div class="sec-row">
                                                <span class="sec-column-1">
                                                    <img src="./assets/images/img/new_img/abs-iso.png" />
                                                </span>
                                                <span class="sec-column-2">
                                                    <p>An ISO 9001:2015 Certified Company From American Bureau of
                                                        Shipping (ABS)</p>
                                                </span>
                                            </div>
                                        </li>

                                        <li>
                                            <h5 class="box-item-title">OSHAS 18001:2007</h5>
                                            <div class="sec-row">
                                                <span class="sec-column-1">
                                                    <img src="./assets/images/img/new_img/qrs_iso.jpg" />
                                                </span>
                                                <span class="sec-column-2">
                                                    <p>An OSHAS 18001:2007 Certified Company From Quality Register
                                                        Systems (QRS)</p>
                                                </span>
                                            </div>
                                        </li>

                                        <li>
                                            <h5 class="box-item-title">In Country Value</h5>
                                            <div class="sec-row">
                                                <span class="sec-column-1">
                                                    <img src="./assets/images/img/new_img/icv_iso.jpg" />
                                                </span>
                                                <span class="sec-column-2">
                                                    <p>An In Country Value (ICV) Certified Company From ADNOC</p>
                                                </span>
                                            </div>
                                        </li>
                                    </ul>
                                    <img style="width:100%; margin-bottom: 15px;"
                                        src="assets/images/img/new_img/about_image-1.jpg" />

                                </div>
                            </div>
                            <!-- <div class="widget recent-portfolio"> -->
                            <!-- <div class="widget-title bottom-10"> -->
                            <!-- <h4><span>Our <strong>Brands</strong></span></h4> -->
                            <!-- </div>  -->
                            <!-- <div class="widget-content"> -->
                            <!-- <ul class="list-unstyled row onepixel"> -->
                            <!-- <li class="col-md-6"> -->
                            <!-- <a href="#"> -->
                            <!-- <img class="img-responsive" src="assets/images/img/portfolio-4.jpg" alt=""> -->
                            <!-- </a> -->
                            <!-- </li> -->
                            <!-- <li class="col-md-6"> -->
                            <!-- <a href="#"> -->
                            <!-- <img class="img-responsive" src="assets/images/img/portfolio-41.jpg" alt=""> -->
                            <!-- </a> -->
                            <!-- </li> -->
                            <!-- <li class="col-md-6"> -->
                            <!-- <a href="#"> -->
                            <!-- <img class="img-responsive" src="assets/images/img/portfolio-42.jpg" alt=""> -->
                            <!-- </a> -->
                            <!-- </li> -->
                            <!-- <li class="col-md-6"> -->
                            <!-- <a href="#"> -->
                            <!-- <img class="img-responsive" src="assets/images/img/portfolio-43.jpg" alt=""> -->
                            <!-- </a> -->
                            <!-- </li> -->
                            <!-- </ul>  -->
                            <!-- </div>  -->
                            <!-- </div>  -->
                        </div>
                    </div>
                </div>

                <section>
                    <div id="portfoliologo" class="section medium bg cover top-40 carouselbox">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="nav carousel-control"> <a href="#" class="prev"><i
                                                class="fa fa-angle-left"></i></a> <a href="#" class="next"><i
                                                class="fa fa-angle-right"></i></a> </div>

                                    <div class="row">
                                        <ul class="carousel-logo list-unstyled bottom-0">
                                            <li>
                                                <div class="portfolio-item">
                                                    <div class="col-md-6">
                                                        <div class="portfolio-img"> <img
                                                                src="assets/images/membership/impa-membership.png"
                                                                alt="" class="img-responsive"> </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h3>MEMBER ASSOCIATIONS</h3>
                                                        <p>Elite Safety & Marine Supplies L.L.C <br />has been approved
                                                            as a supplier<br /> member
                                                            of the International Marine <br />Purchasing Association
                                                            (IMPA).</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="portfolio-item">
                                                    <div class="col-md-6">
                                                        <div class="portfolio-img"> <img
                                                                src="assets/images/membership/impa-membership.png"
                                                                alt="" class="img-responsive"> </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h3>MEMBER ASSOCIATIONS</h3>
                                                        <p>Elite Safety & Marine Supplies L.L.C <br />has been approved
                                                            as a supplier<br /> member
                                                            of the International Marine <br />Purchasing Association
                                                            (IMPA).</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="portfolio-item">
                                                    <div class="col-md-6">
                                                        <div class="portfolio-img"> <img
                                                                src="assets/images/membership/impa-membership.png"
                                                                alt="" class="img-responsive"> </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h3>MEMBER ASSOCIATIONS</h3>
                                                        <p>Elite Safety & Marine Supplies L.L.C <br />has been approved
                                                            as a supplier<br /> member
                                                            of the International Marine <br />Purchasing Association
                                                            (IMPA).</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="portfolio-item">
                                                    <div class="col-md-6">
                                                        <div class="portfolio-img"> <img
                                                                src="assets/images/membership/impa-membership.png"
                                                                alt="" class="img-responsive"> </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h3>MEMBER ASSOCIATIONS</h3>
                                                        <p>Elite Safety & Marine Supplies L.L.C <br />has been approved
                                                            as a supplier<br /> member
                                                            of the International Marine <br />Purchasing Association
                                                            (IMPA).</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="portfolio-item">
                                                    <div class="col-md-6">
                                                        <div class="portfolio-img"> <img
                                                                src="assets/images/membership/impa-membership.png"
                                                                alt="" class="img-responsive"> </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h3>MEMBER ASSOCIATIONS</h3>
                                                        <p>Elite Safety & Marine Supplies L.L.C <br />has been approved
                                                            as a supplier<br /> member
                                                            of the International Marine <br />Purchasing Association
                                                            (IMPA).</p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
    <script>
    $(document).ready(function() {
        var next = jQuery(this).find('.prev');
        var prev = jQuery(this).find('.next');
        // Using custom configuration
        jQuery('.carousel-logo').carouFredSel({
            circular: false,
            responsive: true,
            width: 'variable',
            height: "variable",
            align: "center",
            padding: [15],
            margin: [15],
            items: {
                width: 300,
                visible: {
                    min: 1,
                    max: 2
                },
                height: "variable"
            },
            next: next,
            prev: prev,
            scroll: {
                items: 1,
                easing: "jswing",
                duration: 1000,
                pauseOnHover: true
            }
        });
    });
    </script>
</body>

</html>