<!DOCTYPE html>
<html lang="en">

<head>
    <title>LSA, FFA, Calibration in Fujairah, Dubai, Sharjah, Abu Dhabi, UAE - ESMAR</title>
    <meta name="description"
        content="The company with 22 years experience in marine safety, Firefighting and lifesaving equipment, LSA and FFA Servicing, oil spill response and containment solutions and services in the Middle East We have our branches in Musaffah, Dubai Maritime City and Fujairah.">
    <meta name="keywords"
        content="LSA, FFA, Calibration in Fujairah, Dubai, Sharjah, Abu Dhabi, UAE, Life Saving, Fire Fighting, Oil Spill Response, Ship Deck Equipments, Industrial Safety Equipments, Posters and Safety Signs,
Liferaft Service, Life Saving Appliances Service, Fire Fighting Appliances Service, Lifeboat / Rescue Boat Service, Calibration / Trouble Shooting, Hydraulic Division">
    <meta name="DC.title" content=" Elite Safety & Marine Supplies L L C" />
    <meta name="geo.region" content="AE" />
    <meta name="geo.placename" content="Abu Dhabi" />
    <meta name="geo.position" content="24.349352;54.492051" />
    <meta name="ICBM" content="24.349352, 54.492051" />

    <meta name="robots" content="all" />
    <meta name="author" content="Elite Safety & Marine Supplies L L C, esmarsupplies.com">

    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="LSA, FFA, Calibration in Fujairah, Dubai, Sharjah, Abu Dhabi, UAE - ESMAR" />
    <meta property="og:description"
        content="The company with 22 years experience in marine safety, Firefighting and lifesaving equipment, LSA and FFA Servicing, oil spill response and containment solutions and services in the Middle East We have our branches in Musaffah, Dubai Maritime City and Fujairah." />
    <meta property="og:url" content="https://esmarsupplies.com/" />
    <meta property="og:site_name" content="Elite Safety & Marine Supplies L L C" />
    <script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "Elite Safety & Marine Supplies L L C",
        "logo": "https://esmarsupplies.com/assets/images/img/esmar-logo.png",
        "description": "The company with 22 years experience in marine safety, Firefighting and lifesaving equipment, LSA and FFA Servicing,   oil spill response and containment solutions and services in the Middle East We have our branches in Musaffah, Dubai Maritime City and Fujairah.",
        "url": "https://esmarsupplies.com/",
        "image": "https://esmarsupplies.com/assets/images/img/esmar-logo.png",
        "telephone": "+971 2 554 4315",
        "email": "sales@esmarsupplies.com",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Store#58, Plot#8, Musaffah Industrial Area, M20, Abu Dhabi, UAE",
            "addressLocality": "Abu Dhabi",
            "addressRegion": "Abu Dhabi",
            "addressCountry": "United Arab Emirates",
            "postalCode": "92839"
        },
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "4.9",
            "ratingCount": "2219"
        }
    }
    </script>

    <script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "WebSite",
        "@id": "#website",
        "url": "https://esmarsupplies.com/",
        "name": "Elite Safety & Marine Supplies L L C",
        "potentialAction": {
            "@type": "SearchAction",
            "target": "https://esmarsupplies.com/?s={search_term_string}",
            "query-input": "required name=search_term_string"
        }
    }
    </script>
    
    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <?php include_once('assets-files/slider-home.php'); ?>

        <div id="content" role="main">
            <div id="feature" class="section no-padding bg repeat top-40 ">

                <?php include_once('assets-files/section-product-categories.php'); ?>

                <section class="bg-grey">
                    <?php include_once('assets-files/section-our-brands.php'); ?>
                </section>



                <section>
                    <?php include_once('assets-files/section-our-services.php'); ?>
                </section>


                <section class="bg-grey">
                    <?php include_once('assets-files/section-our-approvals.php'); ?>
                </section>

                <?php include_once('assets-files/section-callout.php'); ?>

            </div>
            <?php include_once('assets-files/footer.php'); ?>

        </div>
        <?php include_once('assets-files/footer-imports-home.php'); ?>
        <script>
        var video = document.getElementById("myVideo");
        var btn = document.getElementById("myBtn");

        function myFunction() {
            alert('ss');
            console.log(video);
            if (video.paused) {
                video.play();
                btn.innerHTML = "Pause";
            } else {
                video.pause();
                btn.innerHTML = "Play";
            }
        }
        </script>

</body>

</html>