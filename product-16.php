<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div id="product-detail">
                            <div class="row">
                                <div class="col-md-7 bottom-sm-30">
                                    <div class="product-images">
                                        <img src="assets/images/img/dec_14_life.jpg" alt="" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h2 class="product-title large thin">
                                        PVC rescue boat
                                    </h2>
                                    <div class="product-review bottom-10">
                                        <span class="star-rating star-rating-4"></span>
                                    </div>

                                    <div class="product-amount clearfix">

                                        <div class="field">
                                            <button class="button"><a href="enquiry">Enquire Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="product-content top-40">
                                <div class="tabs main border clearfix">
                                    <ul class="tabNavigation list-unstyled bottom-0 clearfix">
                                        <li class="active"><a data-toggle="tab" href="#tab1dc11"><i
                                                    class="fa fa-align-justify icon-left"></i> Product Description</a>
                                        </li>
                                    </ul>
                                    <div id="tab1dc11" class="tabs-container active">

                                        <p>The 12-feet inflatable boat  is very popular as a tender but also a day use
                                            boat for ripping around your favorite watering hole. It comes with either
                                            the upgraded  high-pressure inflatable air floor. Boats, and can take up to
                                            a 30 HP outboard motor.</p>
                                        <p>inflatable boats can be used for anything from a dinghy, to a fishing boat,
                                            to even a lifeboat or leisure boat. boat is a large boat, but is still
                                            lightweight, and can be easily carried by one person with a wheel dolly.
                                            Great as a fishing boat too! Go boating without stretching your budget! 
                                            Saturn inflatable boats are made of 1100 denier Korean PVC fabric with
                                            polyester support that has excellent resistance to tearing, tension and
                                            breaking. It is a much more heavy-duty PVC fabric than most of our
                                            competitors with 1000 denier PVC boats.</p>

                                        <p>All around rope grab line. Separate internal air chambers. One-way drain
                                            valve with plug.
                                            All around, durable rubber strike. Safety valve to prevent over inflation.
                                            Stainless steel D-rings for easy towing. Reliable flat air valves with
                                            pushpin design. Front bow D-ring with integral lifting handle. Spare rope
                                            for attaching anchor is included.Double layer of fabric at the bottom of
                                            tubes.</p>
                                        <p>4-piece marine grade plywood floor. Holds up to 5 persons maximum or 4
                                            comfortably.
                                            Deep inflatable V-keel with a rub strake protector.24mm thick heavy-duty
                                            marine grade plywood transom. Extremely rigid, lightweight hull for
                                            excellent performance. Lightweight and strong removable aluminum seat
                                            benches. Free second bench is included when order is placed on-line!
                                            Equipment: aluminum oars, repair kit, seat bench and carry bag. Free,
                                            complimentary generic high-volume hand pump is included.

                                        </p>


                                        </p>
                                    </div>
                                </div>
                            </div>
                            <p>
                        </div>

                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Life Saving</a></li>
                                        <li><a href="firefighting">Fire Fighting</a></li>
                                        <li><a href="chemical-spill">Oil Spill Response</a></li>
                                        <li><a href="ship-deck">Ship Deck Equipments</a></li>
                                        <li><a href="respiratory-protection">Industrial Safety Equipments</a></li>
                                        <li><a href="posters-signs">Posters and Safety Signs</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>