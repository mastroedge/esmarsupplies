<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>
        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div class="row">
                            <div class="heading-area">
                                <h3 class="bottom-0 heading-title wow fadeInDown" data-wow-duration="0.6s"
                                    data-wow-delay="0.6s">
                                    Firefighting Equipment’s</h3>

                                <h4 class="heading-subtitle bottom-20">.</h4>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-01">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_1_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-01">Fire Hoses - Inner /
                                                    Outer </a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-02">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_2_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-02">
                                                    <div class="productTitleInBox">Fire Hose Boxes</div>
                                                </a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-03">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_3_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-03">Spanners for Hydrant</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-04">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_4_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-04">Fire Hose Nozzles</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-05">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_5_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-05">Fog Nozzles & Fire Hose
                                                    Nozzle</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-06">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_6_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-06">International Shore
                                                    Connections</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-07">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_7_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-07">Hose Couplings</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-09">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_9_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-09">Fireman’s Outfits </a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-10">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_10_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-10">Fire Blankets </a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-11">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_11_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-11">Fireproof Lifelines</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-12">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_12_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-12">Chemical Protection
                                                    Suits</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-13">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_13_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-13">Fire Extinguishers</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-14">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_15_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-14">Smoke Tester</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-15">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_151_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-15">Smoke Detector</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-16">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_195_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-16">Detector testing kits
                                                </a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-17">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_17_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-17">Fire Alarm Bell</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-18">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_18_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-18">Emergency Exit Light</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-19">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_3_axe.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-19">Fire Axe</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-20">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_20_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-20">Fire extinguisher
                                                    inspection tag</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-21">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_21_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-21">Fire hose reel</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-22">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_22_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-22">Fire Monitor</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-23">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_322_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-23">Fire Bucket</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-24">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_t18_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-24">Fireman Boots</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-25">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_bl_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-25">Break glass with
                                                    hammer</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content"> <a href="firefighting-26">Quick
                                                        view</a> </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_8fe_fire.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="firefighting-26">Fire extinguisher
                                                    cover</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pagenavi medium top-30">

                            </div>
                        </div>
                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Life Saving</a></li>
                                        <li class="active"><a href="firefighting">Fire Fighting</a></li>
                                        <li><a href="chemical-spill">Oil Spill Response</a></li>
                                        <li><a href="ship-deck">Ship Deck Equipments</a></li>
                                        <li><a href="respiratory-protection">Industrial Safety Equipments</a></li>
                                        <li><a href="posters-signs">Posters and Safety Signs</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6"> <a href="#"> <img class="img-responsive"
                                                    src="assets/images/img/portfolio-4.jpg" alt=""> </a> </li>
                                        <li class="col-md-6"> <a href="#"> <img class="img-responsive"
                                                    src="assets/images/img/portfolio-41.jpg" alt=""> </a> </li>
                                        <li class="col-md-6"> <a href="#"> <img class="img-responsive"
                                                    src="assets/images/img/portfolio-42.jpg" alt=""> </a> </li>
                                        <li class="col-md-6"> <a href="#"> <img class="img-responsive"
                                                    src="assets/images/img/portfolio-43.jpg" alt=""> </a> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>



</body>

</html>