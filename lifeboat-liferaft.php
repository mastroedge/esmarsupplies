<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div class="row">
                            <div class="heading-area">
                                <h3 class="bottom-0 heading-title wow fadeInDown" data-wow-duration="0.6s"
                                    data-wow-delay="0.6s">Life
                                    Saving</h3>

                                <h4 class="heading-subtitle bottom-20">.</h4>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content">
                                                    <a href="inflatable-life-rafts">Quick view</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_1_a.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="inflatable-life-rafts">Liferaft</a></h4>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content">
                                                    <a href="product-2">Quick view</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/dec_01_life.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="product-2">Hydrostatic release unit</a>
                                            </h4>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content">
                                                    <a href="product-4">Quick view</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/dec_02_life.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="product-4">Life Jackets</a></h4>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content">
                                                    <a href="product-5">Quick view</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/dec_03_life.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="product-5">Inflatable Life Jackets</a>
                                            </h4>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content">
                                                    <a href="product-6">Quick view</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/dec_04_life.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="product-6">Lifejacket Lights</a></h4>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content">

                                                    <a href="product-7">Quick view</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/product_7_a.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="product-7">Lifebuoys</a></h4>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content">
                                                    <a href="product-8">Quick view</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/dec_05_life.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="product-8">Lifebuoy Light </a></h4>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content">
                                                    <a href="product-9">Quick view</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/dec_06_life.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="product-9">Immersion Suits </a></h4>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content">
                                                    <a href="product-10">Quick view</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/dec_07_life.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="product-10">Thermal Protective Aid</a>
                                            </h4>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content">
                                                    <a href="product-11">Quick view</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/dec_08_life.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="product-11">Retro Reflective Tape</a>
                                            </h4>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content">
                                                    <a href="product-111">Quick view</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/dec_09_life.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="product-111">Emergency escape breathing
                                                </a></h4>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 bottom-30">
                                <div class="product-item">
                                    <div class="product-images">
                                        <div class="product-mark">
                                            <div class="product-mark-inner">
                                                <div class="product-mark-inner-content">
                                                    <a href="product-12">Quick view</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="assets/images/img/dec_10_life.jpg" alt=""
                                            class="front-end img-responsive">
                                    </div>
                                    <div class="product-info">
                                        <div class="product-title-area clearfix">
                                            <h4 class="product-title"><a href="product-12">Lifeboat Canopy Light</a>
                                            </h4>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="pagenavi medium top-30">
                                <ul class="list-unstyled bottom-0 clearfix">
                                    <li><span>1</span></li>
                                    <li><a href="lifeboat-liferaft-2">2</a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li class="active"><a href="lifeboat-liferaft">Life Saving</a></li>
                                        <li><a href="firefighting">Fire Fighting</a></li>
                                        <li><a href="chemical-spill">Oil Spill Response</a></li>
                                        <li><a href="ship-deck">Ship Deck Equipments</a></li>
                                        <li><a href="respiratory-protection">Industrial Safety Equipments</a></li>
                                        <li><a href="posters-signs">Posters and Safety Signs</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="#">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>



</body>

</html>