<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ESMAR - An Environmental Safety & Marine Firefighting Company</title>

    <?php include_once('assets-files/header-imports-top.php'); ?>
    <?php include_once('assets-files/header-imports.php'); ?>
</head>

<body>
    <div id="wrap">
        <?php include_once('assets-files/header.php'); ?>

        <div id="page" class="top-30">
            <div class="container" id="blog-archive">
                <div class="row" id="content-sidebar">
                    <div id="content" class="col-md-8">
                        <div id="product-detail">
                            <div class="row">
                                <div class="col-md-7 bottom-sm-30">
                                    <div class="product-images">
                                        <img src="assets/images/img/product_iq_01.jpg" alt="" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h2 class="product-title large thin">Hand operated siren</h2>
                                    <div class="product-review bottom-10">
                                        <span class="star-rating star-rating-4"></span>
                                    </div>

                                    <p class="top-20 bottom-20">Product Description</p>
                                    <div class="product-amount clearfix">

                                        <div class="field">
                                            <button class="button"><a href="enquiry">Enquire Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content top-40">
                                <div class="tabs main border clearfix">
                                    <ul class="tabNavigation list-unstyled bottom-0 clearfix">
                                        <li class="active"><a data-toggle="tab" href="#tab1dc11"><i
                                                    class="fa fa-align-justify icon-left"></i> Product Description</a>
                                        </li>
                                    </ul>
                                    <div id="tab1dc11" class="tabs-container active">
                                        <p>
                                            Hand operated siren LK-120 is designed to provide effective warning in
                                            applications where there is no power supply.</p>
                                        <p>Powered by rotation the handle, this model has a plate to shut off the sound
                                            once up to speed giving it the ability to produce different tones
                                            Robust and compact, the siren can be easily transported to remote locations
                                            and folds up to a small size for ease of handling</p>

                                        <p><strong>Specification</strong> </p>

                                        <p>Sound Rating: </p>
                                        <p>120±2dB(A) @1M ,Output Frequency: ,600±20Hz (Dependent on Rotation Speed)</p>
                                        <p>Sound range: 1500M,Net Weight: 9Kg ,Carton Size: 46x37x58CM </p>

                                        <p>Features : no power supply required ,robust and compact for portability</p> 
                                        <p>powerful low frequency sound, universally recognized signal </p>
                                        <p>Shutter mechanism to provide a choice of signals: continuous and warble</p> 
                                        </p>
                                        <p>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="sidebar" class="col-md-4">
                        <div class="sidebar-content">
                            <div class="widget category">
                                <div class="widget-title bottom-10">
                                    <h4><span><strong>Our</strong> Products</span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul>
                                        <li><a href="lifeboat-liferaft">Lifeboat/Life raft (Accessories)</a></li>
                                        <li><a href="respiratory-protection">Respiratory Protection/Public Gas
                                                Detector</a></li>
                                        <li><a href="marine-lights">Marine Lights</a></li>
                                        <li><a href="firefighting">Firefighting Equipment’s</a></li>
                                        <li><a href="ppe-items">Safety / PPE Items</a></li>
                                        <li><a href="valves">Valves, Fittings And Flangess</a></li>
                                    </ul>
                                </div>
                            </div>


                            <div class="widget recent-portfolio">
                                <div class="widget-title bottom-10">
                                    <h4><span>Our <strong>Brands</strong></span></h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list-unstyled row onepixel">
                                        <li class="col-md-6">
                                            <a href="portfolio-detail">
                                                <img class="img-responsive" src="assets/images/img/portfolio-4.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="portfolio-detail">
                                                <img class="img-responsive" src="assets/images/img/portfolio-41.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="portfolio-detail">
                                                <img class="img-responsive" src="assets/images/img/portfolio-42.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li class="col-md-6">
                                            <a href="portfolio-detail">
                                                <img class="img-responsive" src="assets/images/img/portfolio-43.jpg"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('assets-files/footer.php'); ?>
    </div>
    <?php include_once('assets-files/footer-imports.php'); ?>
</body>

</html>